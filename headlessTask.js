import RealmService from './src/core/services/realm/RealmService';
import {NotificationConfigSchema} from './src/core/services/realm/schemas/NotificationConfigSchema';
import GoalStore from './src/core/stores/GoalStore';
import {UserActivitySchema} from './src/core/services/realm/schemas/UserActivitySchema';
import {formatDate} from './src/core/services/DateService';
import moment from 'moment';
import NotificationService from './src/core/services/notifications/NotificationService';
import {NotificationType} from './src/core/services/notifications/NotificationType';
import CommentStore from './src/core/stores/CommentStore';
import {getFlowerIntervalIndex} from './src/screens/HomeScreen/modules/FlowerView';
import BackgroundFetch from "react-native-background-fetch";

export const headlessTask = async ({ taskId }) => {
    try {
        const notificationsConfig = RealmService.getInstance().find({
            schemaName: NotificationConfigSchema.name,
            predicate: 'id = 0'
        })[0];
        if (notificationsConfig && notificationsConfig.is_notifications_enabled) {
            const goalStore = new GoalStore();

            if (RealmService.getInstance().find({
                schemaName: UserActivitySchema.name,
                predicate: `activity > ${formatDate(moment(new Date()).add(-1, 'd').startOf('d').toDate())} and activity < ${formatDate(moment(new Date()).add(-1, 'd').endOf('d').toDate())}`
            }).length === 0) {
                NotificationService.processPush(NotificationType.NO_ACTIVITY_YESTERDAY);
            }

            if (!goalStore.user_goals || (goalStore.user_goals && goalStore.user_goals.length === 0)) {
                NotificationService.processPush(NotificationType.NO_ACTIVE_GOALS);
            } else {
                const commentsStore = new CommentStore();
                const non_commented_goal = goalStore.user_goals.find(g => {
                    return !commentsStore.hasGoalCommentToday(g.id);
                });
                if (non_commented_goal && goalStore.user_goals.length > 0) {
                    NotificationService.processPush(NotificationType.NO_COMMENT);
                }
                goalStore.user_goals.forEach((goal) => {
                    if (moment(goal.ends_at).format('DD.MM.YYYY') === moment(new Date()).add(1, 'd').endOf('d').format('DD.MM.YYYY')
                        && !commentsStore.hasGoalCommentToday(goal.id)
                    ) {
                        NotificationService.processPush(NotificationType.GOAL_LAST_DAY);
                    }

                    if (goal.days_for_complete > 1 && getFlowerIntervalIndex((1 + moment(new Date()).startOf('d').diff(moment(goal.created_at).startOf("d"), 'days'))/goal.days_for_complete)
                        !== getFlowerIntervalIndex(moment(new Date()).startOf('d').diff(moment(goal.created_at).startOf("d"), 'days')/goal.days_for_complete)) {
                        NotificationService.processPush(NotificationType.FLOWER_GROWS_UP)
                    }
                })
            }
        }
    } catch (e) {
        //
    }

    BackgroundFetch.finish(taskId);
};
