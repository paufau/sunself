import * as React from "react";
import 'react-native-gesture-handler';
import {StatusBar, View} from "react-native";
import theme from "./src/core/style/theme";
import NavigationContainer from "./src/core/navigation/NavigationContainer";
import PromptView from "./src/core/services/prompt/PromptView";
import StoresProvider from "./src/core/stores/Provider";
import PromptService from "./src/core/services/prompt/PromptService";
import Orientation from "react-native-orientation-locker";
import {LocalizationContext, localizedData} from "./src/core/localization/useLocalization";
import ToastService from "./src/core/services/toast/ToastService";
import {SafeAreaProvider, SafeAreaView} from "react-native-safe-area-context";
import BackgroundFetch from "react-native-background-fetch";
import PushNotification from "react-native-push-notification";
import RealmService from "./src/core/services/realm/RealmService";
import {UserActivitySchema} from "./src/core/services/realm/schemas/UserActivitySchema";
import {getFormattedNow} from "./src/core/services/DateService";
import NotificationService from "./src/core/services/notifications/NotificationService";
import {headlessTask} from "./headlessTask";

interface IState {
  names: { name: string }[];
}

export default class App extends React.Component<any, IState>{
  state = {
    names: [],
  }

  constructor(props: any) {
    super(props);
    Orientation.lockToPortrait();
    StatusBar.setBarStyle("light-content");
    PromptService.init();
    NotificationService.init();
  }

  componentDidMount() {
    PushNotification.configure({
      onNotification: notification => {
        console.log(notification);
      }
    });
    BackgroundFetch.configure({
      minimumFetchInterval: 15,      // <-- minutes (15 is minimum allowed)
      // Android options
      forceAlarmManager: false,      // <-- Set true to bypass JobScheduler.
      stopOnTerminate: false,
      enableHeadless: true,
      startOnBoot: true,
      requiredNetworkType: BackgroundFetch.NETWORK_TYPE_NONE, // Default
      requiresCharging: false,       // Default
      requiresDeviceIdle: false,     // Default
      requiresBatteryNotLow: false,  // Default
      requiresStorageNotLow: false,  // Default
    }, (taskId: string) => {
      headlessTask({taskId});
    });

    RealmService.getInstance().save(UserActivitySchema.name, {
      id: 0,
      activity: getFormattedNow(),
    })
  }

  render() {
    return (
      <View style={{
        flex: 1,
        backgroundColor: theme.pink_dark,
      }}>
        <LocalizationContext.Provider value={localizedData}>
          <StoresProvider>
            <PromptView>
              <ToastService>
                <SafeAreaProvider>
                  <SafeAreaView style={{
                    flex: 1
                  }}>
                    <NavigationContainer/>
                  </SafeAreaView>
                </SafeAreaProvider>
              </ToastService>
            </PromptView>
          </StoresProvider>
        </LocalizationContext.Provider>
      </View>
    );
  }
}
