export enum SupportedLangs {
  en = 'en',
  ru = 'ru',
  be = 'be',
  uk = 'uk',
}
