import {SupportedLangs} from "./SupportedLangs";

const lang = {
  [SupportedLangs.ru]: require('./ru.json'),
  [SupportedLangs.be]: require('./ru.json'),
  [SupportedLangs.uk]: require('./ru.json'),
  [SupportedLangs.en]: require('./en.json'),
}

export default lang;
