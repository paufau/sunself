import * as React from "react";
import {useContext} from "react";
import * as RNLocalize from "react-native-localize";
import lang from "./lang";
import moment from "moment";
import * as _ from 'lodash';
import {NotificationType} from "../services/notifications/NotificationType";
import {SupportedLangs} from "./lang/SupportedLangs";

export interface ILocalizedData {
  tabs: {
    dairy: string;
    main: string;
    profile: string;
  },
  prompts: {
    understand: string;
    disable_prompts: string;
    welcome: string;
    dairy_welcome: string;
    first_goal: string;
  },
  toast: {
    goal_created: string;
    comment_created: string;
  },
  home_screen: {
    add_goal: string;
    add_comment: string;
    no_active_goals: string;
    day: string;
    comment_successfully_added: string;
  },
  add_goal_screen: {
    cancel: string;
    done: string;
    days_for_complete: string;
    what_goal_do_you_want_to_set: string;
  },
  add_comment_screen: {
    enter_comment: string;
  },
  dairy_screen: {
    record_successfully_added: string;
    go_to_completed: string;
    no_goals_with_comments: string;
  },
  goal_screen: {
    delete_goal: string;
    edit_goal: string;
    go_back: string;
    delete_comment: string;
    delete_goal_modal_message: string;
    cancel: string;
    has_no_comments: string;
  },
  completed_goal_screen: {
    completed: string;
    complete_date: string;
    go_to_comments: string;
  },
  profile_screen: {
    prompts: string;
    notifications: string;
    is_enabled: string;
    is_disabled: string;
    disable_prompts: string;
    disable_notifications: string;
    turn_off: string;
  },
  push_notifications: {
    [NotificationType.NO_ACTIVE_GOALS]: string;
    [NotificationType.GOAL_LAST_DAY]: string;
    [NotificationType.FLOWER_GROWS_UP]: string;
    [NotificationType.NO_ACTIVITY_YESTERDAY]: string;
    [NotificationType.NO_COMMENT]: string;
  }
}

export const localizedData = getLocalizedData();
export const LocalizationContext = React.createContext(localizedData);
const defaultLang = SupportedLangs.en;

function getSupportedLangCode(): SupportedLangs {
  const userLocales = RNLocalize.getLocales();

  if (userLocales && userLocales.length > 0) {
    const supportedLangs = Object.keys(lang);
    const locale = userLocales.find(ul => _.includes(supportedLangs, ul.languageCode.toLocaleLowerCase()));
    if (locale) {
      return locale.languageCode as SupportedLangs;
    }
  }
  return defaultLang as SupportedLangs;
}

function getLocalizedData(): ILocalizedData {
  let appLocaleCode = getSupportedLangCode();

  require("moment/min/locales.min");
  moment.locale(appLocaleCode);

  if (appLocaleCode !== defaultLang) {
    return {
      ...lang[defaultLang],
      ...lang[appLocaleCode]
    }
  }

  return lang[appLocaleCode];
}

export default function useLocalization(): ILocalizedData {
  return useContext(LocalizationContext);
}
