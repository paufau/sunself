import {action, observable, runInAction} from "mobx";
import RealmService from "../services/realm/RealmService";
import {GoalSchema} from "../services/realm/schemas/GoalSchema";
import generateID from "../services/IDGenerator";
import moment from 'moment';
import {Pending} from "./Pending";
import {getFormattedToday} from "../services/DateService";
import {GoalCommentSchema} from "../services/realm/schemas/GoalCommentSchema";

export interface IGoal {
  id: string,
  title: string,
  days_for_complete: number,
  created_at: string,
  ends_at: string,
}

export default class GoalStore {
  @observable user_goals?: IGoal[] = undefined;
  @observable completed_goals?: IGoal[] = undefined;
  @observable allGoals?: IGoal[] = undefined;
  @observable pending = observable.map({
    'createNewGoal': Pending.CLEAR,
    'deleteGoal': Pending.CLEAR,
    'editGoal': Pending.CLEAR,
  });

  constructor() {
    runInAction(() => {
      this.updateGoals();
    })
  }

  private updateGoals = () => {
    const realm = RealmService.getInstance();
    this.user_goals = realm.find<IGoal>({
      schemaName: GoalSchema.name,
      predicate: `ends_at >= ${moment().endOf("d").format('YYYY-MM-DD[T]HH:mm:ss')}`,
      sorted: 'ends_at'
    });

    this.completed_goals = realm.find<IGoal>({
      schemaName: GoalSchema.name,
      predicate: `ends_at < ${moment().endOf("d").format('YYYY-MM-DD[T]HH:mm:ss')}`,
      sorted: 'ends_at'
    });

    this.allGoals = [...this.completed_goals, ...this.user_goals];
  }

  @action editGoal = (goal: IGoal) => {
    if (this.pending.get('editGoal') !== Pending.LOADING) {
      this.pending.set('editGoal', Pending.LOADING);
      return new Promise((resolve, reject) => {
        try {
          const new_goal = {
            ...goal,
            title: goal.title.trim(),
            ends_at: moment(goal.created_at, 'YYYY-MM-DD[T]HH:mm:ss').add(goal.days_for_complete - 1, 'd').endOf("d").format('YYYY-MM-DD[T]HH:mm:ss'),
          }

          const realm = RealmService.getInstance();
          realm.save<IGoal>(GoalSchema.name, new_goal);

          this.updateGoals();

          this.pending.set('editGoal', Pending.DONE);
          resolve(new_goal);
        } catch (e) {
          this.pending.set('editGoal', Pending.FAILED);
          reject(e);
        }
      })
    }
    return Promise.reject();
  }

  @action createNewGoal = (title: string, days_for_complete: number) => {
    if (this.pending.get('createNewGoal') !== Pending.LOADING) {
      this.pending.set('createNewGoal', Pending.LOADING);
      return new Promise<IGoal>((resolve, reject) => {
        try {
          const new_goal: IGoal = {
            title: title.trim(),
            days_for_complete,
            id: generateID(),
            created_at: getFormattedToday(),
            ends_at: moment(new Date()).add(days_for_complete - 1, 'd').endOf("d").format('YYYY-MM-DD[T]HH:mm:ss'),
          };

          const realm = RealmService.getInstance();
          realm.save<IGoal>(GoalSchema.name, new_goal);

          this.updateGoals();

          this.pending.set('createNewGoal', Pending.DONE);
          resolve(new_goal);
        } catch (e) {
          this.pending.set('createNewGoal', Pending.FAILED);
          reject(e);
        }
      })
    }
    return Promise.reject();
  }

  @action deleteGoal = (goal_id: string) => {
    if (this.pending.get('deleteGoal') !== Pending.LOADING) {
      this.pending.set('deleteGoal', Pending.LOADING);
      return new Promise<IGoal>((resolve, reject) => {
        try {
          const realm = RealmService.getInstance();
          realm.deleteById(GoalSchema.name, goal_id);
          realm.deleteByPredicate(GoalCommentSchema.name, `goal_id = "${goal_id}"`);

          this.updateGoals();

          this.pending.set('deleteGoal', Pending.DONE);
          resolve();
        } catch (e) {
          this.pending.set('deleteGoal', Pending.FAILED);
          reject(e);
        }
      })
    }
    return Promise.reject();
  };
}
