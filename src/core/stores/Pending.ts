
export enum Pending {
  CLEAR = 'clear',
  DONE = 'done',
  LOADING = 'loading',
  FAILED = 'failed',
}
