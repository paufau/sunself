import * as React from "react";
import {Provider} from "mobx-react";
import PromptStore from "./PromptStore";
import GoalStore from "./GoalStore";
import CommentStore from "./CommentStore";

export const Stores = {
  prompt: new PromptStore(),
  goals: new GoalStore(),
  comments: new CommentStore(),
}

const StoresProvider: React.FunctionComponent = props => {
  return (
    <Provider {...Stores}>
      {props.children}
    </Provider>
  )
}

export default StoresProvider;
