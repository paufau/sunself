import {action, observable} from "mobx";
import RealmService from "../services/realm/RealmService";
import {Pending} from "./Pending";
import {GoalCommentSchema} from "../services/realm/schemas/GoalCommentSchema";
import generateID from "../services/IDGenerator";
import moment from "moment";
import {getFormattedToday} from "../services/DateService";
import {HasTodayCommentSubscription} from "../components/HasTodayComment/HasTodayComment";
import {GoalCommentsSubscription} from "../../screens/GoalScreen/GoalScreen";

export interface IGoalComment {
  id: string,
  goal_id: string,
  created_at: string,
  commented_date: string,
  text: string,
}

export default class CommentStore {
  @observable pending = observable.map({
    'createComment': Pending.CLEAR,
    'deleteComment': Pending.CLEAR,
  });

  @action createComment = (goal_id: string, commented_date: string, comment_text: string) => {
    if (this.pending.get('createComment') !== Pending.LOADING) {
      this.pending.set('createComment', Pending.LOADING);
      return new Promise<IGoalComment>((resolve, reject) => {
        try {
          const realm = RealmService.getInstance();
          const new_comment = {
            id: generateID(),
            goal_id,
            created_at: getFormattedToday(),
            commented_date,
            text: comment_text.trim(),
          };

          realm.save<IGoalComment>(GoalCommentSchema.name, new_comment);

          this.pending.set('createComment', Pending.DONE);
          HasTodayCommentSubscription.next({
            goal_id,
            has_comment: this.hasGoalCommentToday(goal_id)
          });

          GoalCommentsSubscription.next({
            goal_id,
            comments: this.getAllGoalComments(goal_id),
          });

          resolve(new_comment);
        } catch (e) {
          this.pending.set('createComment', Pending.FAILED);
          reject(e);
        }
      })
    }
    return Promise.reject();
  }

  @action deleteComment = (comment: IGoalComment) => {
    if (this.pending.get('deleteComment') !== Pending.LOADING){
      this.pending.set('deleteComment', Pending.LOADING);
      return new Promise((resolve, reject) => {
        try {
          const realm = RealmService.getInstance();
          realm.deleteById(GoalCommentSchema.name, comment.id);

          HasTodayCommentSubscription.next({
            goal_id: comment.goal_id,
            has_comment: this.hasGoalCommentToday(comment.goal_id)
          });

          GoalCommentsSubscription.next({
            goal_id: comment.goal_id,
            comments: this.getAllGoalComments(comment.goal_id),
          });

          this.pending.set('deleteComment', Pending.DONE);
          resolve();
        } catch (e) {
          this.pending.set('deleteComment', Pending.FAILED);
          reject();
        }
      })
    }
    return Promise.reject();
  }

  @action getAllGoalComments = (goal_id: string): IGoalComment[] => {
    const realm = RealmService.getInstance();
    return realm.find<IGoalComment>({
      schemaName: GoalCommentSchema.name,
      predicate: `goal_id = "${goal_id}"`,
      sorted: 'commented_date'
    })
  }

  @action hasGoalCommentToday = (goal_id: string): boolean => {
    const realm = RealmService.getInstance();
    const todayGoals = realm.find<IGoalComment>({
      schemaName: GoalCommentSchema.name,
      predicate: `goal_id = "${goal_id}" and commented_date > ${moment(new Date()).startOf("d").format('YYYY-MM-DD[T]HH:mm:ss')} and commented_date <= ${moment(new Date()).endOf("d").format('YYYY-MM-DD[T]HH:mm:ss')}`
    })
    return todayGoals.length > 0;
  }
}
