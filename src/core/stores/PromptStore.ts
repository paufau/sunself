import {action, observable} from "mobx";
import {IPrompt, IPromptViewParams} from "../services/prompt/PromptService";

export default class PromptStore {
  @observable public shownPrompt?: IPrompt = undefined;
  @observable public shownPromptViewParams?: IPromptViewParams = undefined;

  @action public setShownPrompt = (prompt?: IPrompt) => {
    this.shownPrompt = prompt;
  }

  @action public setShownPromptViewParams = (params?: IPromptViewParams) => {
    this.shownPromptViewParams = params;
  }
}
