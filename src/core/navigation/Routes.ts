export enum Routes {
  HomeScreen = 'HomeScreen',
  DairyScreen = 'DairyScreen',
  ProfileScreen = 'ProfileScreen',
  AddGoalScreen = 'AddGoalScreen',
  AddCommentScreen = 'AddCommentScreen',
  GoalScreen = 'GoalScreen',
  CompletedGoalScreen = 'CompletedGoalScreen'
}
