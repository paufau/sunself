import * as React from 'react';
import {NavigationContainer as Navigator} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Text} from "react-native";
import {BottomTabBarButtonProps} from "@react-navigation/bottom-tabs/lib/typescript/src/types";
import TabBarButton from "./TabBarButton";
import useLocalization from "../localization/useLocalization";
import {Routes} from "./Routes";
import DairyIcon from "./icons/DairyIcon";
import DairyScreen from "../../screens/DairyScreen/DairyScreen";
import theme from "../style/theme";
import HomeIcon from "./icons/HomeIcon";
import ProfileScreen from "../../screens/ProfileScreen/ProfileScreen";
import HomeScreen from "../../screens/HomeScreen/HomeScreen";
import AddGoalScreen from "../../screens/AddGoalScreen/AddGoalScreen";
import AddCommentScreen from "../../screens/AddCommentScreen/AddCommentScreen";
import {IGoal} from "../stores/GoalStore";
import GoalScreen from "../../screens/GoalScreen/GoalScreen";
import CompletedGoalScreen from "../../screens/CompletedGoalScreen/CompletedGoalScreen";
import ProfileIcon from "./icons/ProfileIcon";

export type DairyStackNavigationParams = {
  [Routes.DairyScreen]: {};
  [Routes.GoalScreen]: {goal: IGoal};
  [Routes.CompletedGoalScreen]: {};
}

function DairyStackNavigator() {
  const DairyStack = createStackNavigator<DairyStackNavigationParams>()

  return (
    <DairyStack.Navigator
      initialRouteName={Routes.DairyScreen}
      headerMode={'none'}
      screenOptions={{
        cardStyle: {
          backgroundColor: "transparent"
        }
      }}
    >
      <DairyStack.Screen name={Routes.DairyScreen} component={DairyScreen}/>
      <DairyStack.Screen name={Routes.GoalScreen} component={GoalScreen}/>
      <DairyStack.Screen name={Routes.CompletedGoalScreen} component={CompletedGoalScreen}/>
    </DairyStack.Navigator>
  )
}

function TabNavigator() {
  const Tab = createBottomTabNavigator();
  const localization = useLocalization();

  const tabs = [
    {
      screen: DairyStackNavigator,
      route_name: 'DairyStack',
      label: localization.tabs.dairy,
      icon: DairyIcon,
    },
    {
      screen: HomeScreen,
      route_name: Routes.HomeScreen,
      label: localization.tabs.main,
      icon: HomeIcon,
    },
    {
      screen: ProfileScreen,
      route_name: Routes.ProfileScreen,
      label: localization.tabs.profile,
      icon: ProfileIcon,
    },
  ]

  const tabHeight = 80;

  return (
    <Tab.Navigator
      tabBarOptions={{
        style: {
          backgroundColor: theme.pink_dark,
          borderTopWidth: 0,
          elevation: 0,
          height: tabHeight,
          paddingHorizontal: '8%',
        },
        tabStyle: {
          height: tabHeight,
          padding: 0,
          margin: 0,
          paddingBottom: 20,
        },
        safeAreaInsets: {
          bottom: 0,
          top: 0
        },
      }}
      initialRouteName={Routes.HomeScreen}
    >
      {tabs.map((tab, index) => {
        return (
          <Tab.Screen
            key={index}
            options={{
              tabBarButton: (props: BottomTabBarButtonProps) => {
                return <TabBarButton props={props}/>
              },
              tabBarLabel: ({focused}) => {
                return (
                  <Text style={{
                    fontSize: 10,
                    lineHeight: 12,
                    marginTop: 2,
                    color: focused ? theme.green : theme.white,
                  }}>
                    {tab.label}
                  </Text>
                )
              },
              tabBarIcon: ({focused}) => {
                return (
                  <tab.icon
                    height={48}
                    selected={focused}
                  />
                )
              },
            }}
            name={tab.route_name}
            component={tab.screen}
          />
        )
      })}
    </Tab.Navigator>
  )
}

export type RootStackNavigationParams = {
  [Routes.AddCommentScreen]: { goal: IGoal; };
  [Routes.AddGoalScreen]: {goal?: IGoal};
  Tab: {};
}

export default function NavigationContainer() {
  const Stack = createStackNavigator<RootStackNavigationParams>();

  return (
    <Navigator>
      <Stack.Navigator
        initialRouteName={'Tab'}
        headerMode={'none'}
      >
        <Stack.Screen name={'Tab'} component={TabNavigator}/>
        <Stack.Screen name={Routes.AddGoalScreen} component={AddGoalScreen}/>
        <Stack.Screen name={Routes.AddCommentScreen} component={AddCommentScreen}/>
      </Stack.Navigator>
    </Navigator>
  );
}
