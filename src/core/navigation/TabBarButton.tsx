import * as React from 'react';
import {BottomTabBarButtonProps} from "@react-navigation/bottom-tabs/lib/typescript/src/types";
import {TouchableOpacity, View} from "react-native";

interface IProps {
  props: BottomTabBarButtonProps;
}

export default class TabBarButton extends React.Component<IProps, any>{
  render() {
    const {
      props
    } = this.props;

    return (
      <View style={{
        flex: 1,
      }}>
        <TouchableOpacity
          {...props}
        >

        </TouchableOpacity>
      </View>
    );
  }
}
