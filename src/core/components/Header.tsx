import * as React from "react";
import {Dimensions, TouchableOpacity, View} from "react-native";
import Text from "./Text";
import theme from "../style/theme";
import RadialGradientView from "./RadialGradientView";

interface IProps {
  leftButton?: {
    title: string;
    onPress: () => void;
  },
  title: string;
  fontSize?: number;
  lineHeight?: number;
}

const Header: React.FunctionComponent<IProps> = props => {
  return (
    <React.Fragment>
      <View style={{
        width: '100%',
        paddingBottom: Dimensions.get("window").height * 0.018,
        paddingTop: Dimensions.get("window").height * 0.035,
        flexDirection: 'row',
        alignItems: 'center'
      }}>
        <View style={{
          flex: 1,
        }}>
          {props.leftButton ? (
            <TouchableOpacity onPress={props.leftButton.onPress} style={{
              paddingLeft: Dimensions.get("window").width * 0.1,
              flex: 1,
              minHeight: 34,
              justifyContent: 'center',
            }}>
              <Text style={{
                fontSize: 12,
                lineHeight: 14,
                color: theme.rgba(theme.white, 0.4)
              }}>{props.leftButton.title}</Text>
            </TouchableOpacity>
          ) : null}
        </View>
        <Text style={{
          fontSize: Dimensions.get("window").height * (props.fontSize ? props.fontSize/812 : 0.024),
          lineHeight: Dimensions.get("window").height * (props.lineHeight ? props.lineHeight/812 : 0.028)
        }}>
          {props.title}
        </Text>
        <View style={{flex: 1}}/>
      </View>
      <RadialGradientView width={Dimensions.get("window").width}/>
    </React.Fragment>
  )
}

export default Header;
