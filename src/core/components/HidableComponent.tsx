import * as React from "react";
import {Animated, Easing, StyleProp, ViewStyle} from "react-native";

interface IProps {
  children: React.ReactNode;
  mustInitialHide?: boolean;
  hideAnimationConfig?: object;
  showAnimationConfig?: object;
  style?: StyleProp<ViewStyle>;
}

interface IState {
  isFocused: boolean;
}

export default class HidableComponent extends React.Component<IProps, IState>{
  constructor(props: IProps) {
    super(props);

    this.state = {
      isFocused: !Boolean(props.mustInitialHide),
    }

    this.animation = new Animated.Value(!Boolean(props.mustInitialHide) ? 1 : 0);
  }

  private animation: Animated.Value;

  public show = (callback?: () => void) => {
    this.setState({isFocused: true}, () => {
      Animated.timing(this.animation, {
        toValue: 1,
        duration: 300,
        useNativeDriver: true,
        easing: Easing.inOut(Easing.sin),
        ...(this.props.showAnimationConfig || {}),
      }).start(callback);
    })
  }

  public hide = (callback?: (result: Animated.EndResult) => void) => {
    Animated.timing(this.animation, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true,
      easing: Easing.inOut(Easing.sin),
      ...(this.props.hideAnimationConfig || {}),
    }).start((result) => {
      if (result.finished) {
        this.setState({isFocused: false})
      }
      callback && callback(result);
    })
  }

  render() {
    return (
      <Animated.View style={[{
        zIndex: 1,
        opacity: this.animation,
      }, this.props.style]}>
        {this.props.children}
      </Animated.View>
    );
  }
}
