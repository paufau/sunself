import * as React from "react";
import {PanGestureHandler, PanGestureHandlerGestureEvent, State, TapGestureHandler} from "react-native-gesture-handler";
import {Animated, Dimensions, Easing, StyleProp, View, ViewStyle} from "react-native";

interface IProps {
  data: any[];
  style: StyleProp<ViewStyle>;
  itemWidth: number;
  initialIndex?: number;
  disableInertia?: boolean;
  maxItemsForInertiaScrolling?: number;
  onItemCentered?: (index: number) => void;
  renderItem: (item: any, index: number) => React.ReactNode;
  overlay?: React.ReactNode;
  leftThreshold?: React.ReactNode;
  rightThreshold?: React.ReactNode;
  onMovementStart?: () => void;
}

interface IState {

}

export default class HorizontalCenterSnapList extends React.Component<IProps, IState>{
  constructor(props: IProps) {
    super(props);
    this.init(props);
  }

  shouldComponentUpdate(nextProps: Readonly<IProps>, nextState: Readonly<IState>, nextContext: any): boolean {
    if (JSON.stringify(nextProps.data) !== JSON.stringify(this.props.data)) {
      this.init(nextProps);
      return true;
    }
    return false;
  }

  private init = (props: IProps) => {
    this.DATA_LENGTH = props.data.length;
    this.ITEM_WIDTH = props.itemWidth;
    this.CONTENT_WIDTH = (this.DATA_LENGTH - 1) * this.ITEM_WIDTH + this.SCREEN_WIDTH;

    if (props.initialIndex !== undefined) {
      const offset = this.computePosition(props.initialIndex * this.ITEM_WIDTH * -1);
      this.translateX.setValue(offset);
      this.endTranslatePosition = offset;
    }
  }

  private translateX = new Animated.Value(0);
  private endTranslatePosition = 0;
  private prevTapState: State = State.END;
  private prevPanState: State = State.END;

  private SCREEN_WIDTH = Dimensions.get("window").width;
  private DATA_LENGTH: number = 0;
  private ITEM_WIDTH: number = 0;
  private CONTENT_WIDTH: number = 0;

  public snapToNumberOfItems = (count: number) => {
    this.snapToX(this.endTranslatePosition - (count * this.ITEM_WIDTH));
  };

  private computePosition = (value: number) => {
    if (value > 0) {
      return 0;
    }
    if (value < -this.CONTENT_WIDTH + this.SCREEN_WIDTH) {
      return -this.CONTENT_WIDTH + this.SCREEN_WIDTH;
    }
    return value;
  }

  private computeInertia = (value: number) => {
    const absValue = Math.abs(value - (this.ITEM_WIDTH / 2));
    const direction = value < 0 ? -1 : 1;
    const numOfItems = Math.trunc(absValue / this.ITEM_WIDTH)
    this.props.onItemCentered && this.props.onItemCentered(numOfItems);
    return this.ITEM_WIDTH * numOfItems * direction;
  }

  private snapToX = (index: number) => {

    const listenerID = this.translateX.addListener((value) => {
      this.endTranslatePosition = value.value;
    })

    const animateToValue = this.computeInertia(this.computePosition(index));
    Animated.timing(this.translateX, {
      toValue: animateToValue,
      duration: 200,
      easing: Easing.inOut(Easing.sin),
      useNativeDriver: true,
    }).start((result) => {
      if (result.finished) {
        this.endTranslatePosition = animateToValue;
      }
      this.translateX.removeListener(listenerID);
    });
  }

  render() {
    const {style, overlay, data, renderItem, leftThreshold, rightThreshold} = this.props;
    return (
      <TapGestureHandler onHandlerStateChange={event => {
        if (event.nativeEvent.state === State.END && this.prevTapState === State.ACTIVE) {
          this.translateX.stopAnimation();
          this.snapToX(this.endTranslatePosition - event.nativeEvent.x + (this.SCREEN_WIDTH / 2));
        }
        this.prevTapState = event.nativeEvent.state;
      }}>
        <PanGestureHandler
          minDist={20}
          onHandlerStateChange={event => {
            if (this.prevPanState === State.BEGAN && event.nativeEvent.state === State.ACTIVE) {
              this.props.onMovementStart && this.props.onMovementStart();
            }

            if (this.prevPanState === State.ACTIVE && event.nativeEvent.state === State.END) {
              this.endTranslatePosition += event.nativeEvent.translationX;
              const velocity = Math.trunc(event.nativeEvent.velocityX / 100);
              const direction = event.nativeEvent.velocityX < 0 ? -1 : 1;
              let moveTo = velocity * velocity / 2;

              if (this.props.disableInertia) {
                moveTo = 0;
              } else if (this.props.maxItemsForInertiaScrolling && moveTo > (this.ITEM_WIDTH * this.props.maxItemsForInertiaScrolling - this.ITEM_WIDTH / 2)) {
                moveTo = this.props.maxItemsForInertiaScrolling * this.ITEM_WIDTH - this.ITEM_WIDTH / 2;
              }

              const listenerID = this.translateX.addListener((value) => {
                this.endTranslatePosition = value.value;
              })

              const animateToValue = this.computeInertia(this.computePosition(this.endTranslatePosition + (moveTo * direction)));

              Animated.spring(this.translateX, {
                toValue: animateToValue,
                bounciness: 0,
                velocity: velocity,
                useNativeDriver: true,
              }).start((result) => {
                if (result.finished) {
                  this.endTranslatePosition = animateToValue;
                }
                this.translateX.removeListener(listenerID);
              });
            }
            this.prevPanState = event.nativeEvent.state;
          }}
          onGestureEvent={(event: PanGestureHandlerGestureEvent) => {
            this.translateX.setValue(this.computePosition(event.nativeEvent.translationX + this.endTranslatePosition));
          }}
        >
          <View style={[{
            position: 'relative',
          }, style]}>
            {overlay ? (
              <View style={{
                position: 'absolute',
                width: '100%',
                height: '100%',
                zIndex: 2,
              }}>
                {overlay}
              </View>
            ) : null}
            <Animated.View style={{
              transform: [{
                translateX: this.translateX
              }],
              width: this.CONTENT_WIDTH,
            }}>
              <View style={{
                flexDirection: 'row',
                width: this.CONTENT_WIDTH
              }}>
                <View style={{
                  width: (this.SCREEN_WIDTH / 2) - (this.ITEM_WIDTH / 2),
                  alignItems: 'flex-end',
                }}>
                  {leftThreshold ? leftThreshold : null}
                </View>
                {data.map((item, index) => {
                  return (
                    <React.Fragment key={index}>
                      {renderItem(item, index)}
                    </React.Fragment>
                  )
                })}
                <View style={{
                  width: (this.SCREEN_WIDTH / 2) - (this.ITEM_WIDTH / 2),
                  alignItems: 'flex-start',
                }}>
                  {rightThreshold ? rightThreshold : null}
                </View>
              </View>
            </Animated.View>
          </View>
        </PanGestureHandler>
      </TapGestureHandler>
    );
  }
}
