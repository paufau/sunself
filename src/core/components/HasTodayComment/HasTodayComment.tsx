import * as React from "react";
import Observable from "../Observable";
import CommentStore from "../../stores/CommentStore";
import {Stores} from "../../stores/Provider";

export const HasTodayCommentSubscription = new Observable<undefined | { goal_id: string, has_comment: boolean;}>(undefined);

interface IProps {
  goal_id: string;
  children: (hasTodayComment: boolean) => React.ReactNode;
  comments?: CommentStore;
}

interface IState {
  has_today_comment: boolean;
}

export default class HasTodayComment extends React.Component<IProps, IState>{
  private subID: number;
  constructor(props: IProps) {
    super(props);
    this.state = {
      has_today_comment: Stores.comments.hasGoalCommentToday(props.goal_id)
    }
    this.subID = HasTodayCommentSubscription.subscribe(nextData => {
      if (nextData && nextData.goal_id === props.goal_id) {
        this.setState({has_today_comment: nextData.has_comment})
      }
    })
  }

  componentWillUnmount() {
    HasTodayCommentSubscription.describe(this.subID);
  }

  render() {
    return this.props.children(this.state.has_today_comment);
  }
}
