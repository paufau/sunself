import React from "react";
import Svg, {Defs, RadialGradient, Rect, Stop} from "react-native-svg";
import {IIconProps} from "../navigation/icons/DairyIcon";

const RadialGradientView = (props: IIconProps) => {
  return (
    <Svg width={props.width} height="3" viewBox="0 0 375 3" fill="none">
      <Rect width="375" height="3" fill="url(#paint0_radial)"/>
      <Defs>
        <RadialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse"
                        gradientTransform="translate(187.5 1.5) rotate(90) scale(1.5 187.5)">
          <Stop stopColor="#8BD67F"/>
          <Stop offset="1" stopColor="#8BD67F" stopOpacity="0"/>
        </RadialGradient>
      </Defs>
    </Svg>
  );
}

export default RadialGradientView;
