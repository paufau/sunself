import * as React from "react";
import {TextProps, Text as NativeText} from "react-native";
import theme from "../style/theme";
import Fonts from "../style/Fonts";

export const defaultTextStyle = {
  fontSize: 12,
  lineHeight: 14,
  color: theme.white,
  fontFamily: Fonts.TenorSans,
}

const Text: React.FunctionComponent<TextProps> = props => {
  return (
    <NativeText {...props} style={[defaultTextStyle, props.style]}/>
  );
}

export default Text;
