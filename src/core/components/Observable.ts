interface ISubscriber<T> {
  id: number;
  callback: (data: T) => void;
}

export default class Observable<T> {
  public value: T;

  private nextID = 0;
  private subscribers: ISubscriber<T>[] = [];

  constructor(defaultValue: T) {
    this.value = defaultValue;
  }

  public subscribe = (callback: (nextData: T) => void): number => {
    const ID = this.nextID + 1;
    this.subscribers.push({
      id: ID,
      callback: callback,
    })
    return ID;
  }

  public describe = (ID: number) => {
    this.subscribers = this.subscribers.filter(s => s.id !== ID);
  }

  public next = (nextValue: T) => {
    this.value = nextValue;
    this.subscribers.forEach(s => {
      s.callback(nextValue);
    })
  }
}
