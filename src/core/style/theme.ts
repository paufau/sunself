import Color from 'color';

const defaultTheme = {
  white: '#fff',
  black: '#000',
  dark: '#333333',
  green: '#8BD67F',
  pink: '#695E81',
  pink_dark: '#595269',
  red: '#F86262',

  rgba: (color: string, alpha: number): string => {
    const c = Color(color);
    return `rgba(${c.red()}, ${c.green()}, ${c.blue()}, ${alpha})`;
  }
}

const theme = defaultTheme;

export default theme;
