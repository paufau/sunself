import * as React from "react";
import {Dimensions, View} from "react-native";
import theme from "../../style/theme";
import IntersectIcon from "./icons/IntersectIcon";
import Text from "../../components/Text";
import HidableComponent from "../../components/HidableComponent";

interface IProps {

}

interface IState {
  isShown: boolean;
  title: string;
}

export default class ToastService extends React.Component<IProps, IState>{
  state = {
    isShown: false,
    title: ''
  }

  constructor(props: IProps) {
    super(props);
    ToastService.thisRef = this;
  }

  public static open = (title: string) => {
    ToastService.thisRef && ToastService.thisRef._open(title);
  }

  private static thisRef: ToastService | null = null;

  private ToastRef: HidableComponent | null = null;

  private _open = (title: string) => {
    this.setState({isShown: true, title}, () => {
      this.ToastRef && this.ToastRef.show();
      setTimeout(this._close, 2000)
    })
  }

  private _close = () => {
    this.ToastRef && this.ToastRef.hide(() => {
      this.setState({isShown: false});
    })
  }

  render() {
    return (
      <View style={{
        flex: 1,
        position: 'relative'
      }}>
          {this.state.isShown ? (
            <View pointerEvents={"none"} style={{
              position: 'absolute',
              left: 0,
              right: 0,
              bottom: '50%',
              zIndex: 1,
            }}>
              <HidableComponent
                mustInitialHide
                hideAnimationConfig={{
                  duration: 1400,
                }}
                ref={ref => this.ToastRef = ref}
              >
                <View style={{
                  height: 60,
                  width: Dimensions.get("window").width,
                  backgroundColor: theme.green,
                  position: 'relative'
                }}>
                  <View style={{
                    left: Dimensions.get("window").width * 0.12,
                    top: 0,
                    bottom: 0,
                    position: 'absolute'
                  }}>
                    <IntersectIcon/>
                  </View>
                  <View style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                    zIndex: 1,
                  }}>
                    <Text>{this.state.title}</Text>
                  </View>
                </View>
              </HidableComponent>
            </View>
          ) : null}

        {this.props.children}
      </View>
    );
  }
}
