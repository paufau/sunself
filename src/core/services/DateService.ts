import moment from "moment";

export default function getArrayOfDates(dateStart: Date, dateEnd: Date): Date[] {
  let days = [];
  let day = moment(dateStart)
  let _dateEnd = moment(dateEnd);

  while (day <= _dateEnd) {
    days.push(day.toDate());
    day = day.clone().add(1, 'd');
  }

  return days;
}

export function getFormattedToday() {
  return moment(new Date()).startOf("d").format('YYYY-MM-DD[T]HH:mm:ss');
}

export function getFormattedNow() {
  return moment(new Date()).format('YYYY-MM-DD[T]HH:mm:ss');
}

export function formatDate(date: Date) {
  return moment(date).format('YYYY-MM-DD[T]HH:mm:ss');
}
