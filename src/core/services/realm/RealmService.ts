import Realm from 'realm';
import Schemas from "./schemas";
import * as _ from 'lodash';

export default class RealmService {
  private static instance?: _RealmService = undefined;
  public static getInstance: () => _RealmService = () => {
    if (RealmService.instance) {
      return RealmService.instance;
    }
    return new _RealmService();
  }
}

export interface IRealmFindParams {
  schemaName: string;
  predicate?: string;
  sorted?: string;
  reverseSorted?: boolean;
  getRealmObjects?: boolean;
}

class _RealmService {
  private realm: Realm;

  constructor() {
    this.realm = new Realm({schema: Schemas, schemaVersion: 1});
  }

  public save = <T>(schemaName: string, params: T) => {
    try {
      this.realm.write(() => {
        if (Array.isArray(params)) {
          params.forEach(p => {
            this.realm.create(schemaName, p, true);
          })
        } else {
          this.realm.create(schemaName, params, true);
        }
      })
    } catch (e) {
      console.warn(e);
    }
  }

  public find = <T>(params: IRealmFindParams): Array<T> => {
    try {
      const {schemaName, predicate, reverseSorted, sorted, getRealmObjects} = params;
      let realmObjects = this.realm.objects(schemaName);
      if (realmObjects) {
        if (predicate) {
          realmObjects = realmObjects.filtered(predicate);
        }
        if (sorted) {
          realmObjects = realmObjects.sorted(sorted, reverseSorted);
        }
        if (getRealmObjects) {
          // @ts-ignore
          return realmObjects;
        }
        return _.values(JSON.parse(JSON.stringify(realmObjects))) as Array<T>;
      }

      console.warn('Nothing has been found!')
      return [];
    } catch (e) {
      console.warn(e);
      return [];
    }
  };

  public deleteById = (schemaName: string, id: number | string | number[] | string[]) => {
    try {
      if (Array.isArray(id)) {
        this.realm.write(() => {
          id.forEach((_id: number | string) => {
            this.realm.delete(this.realm.objectForPrimaryKey(schemaName, _id));
          });
        })
      } else {
        this.realm.write(() => {
          this.realm.delete(this.realm.objectForPrimaryKey(schemaName, id));
        })
      }
    } catch (e) {
      console.log('RealmService method [deleteById]', e);
    }
  };

  public deleteByPredicate = (schemaName: string, predicate: string) => {
    try {
      const removingDataSet = this.find({
        schemaName, predicate,
        getRealmObjects: true,
      });

      if (removingDataSet) {
        this.realm.write(() => {
          Array.from(removingDataSet).forEach(realmObj => {
            this.realm.delete(realmObj);
          });
        })
      }
    } catch (e) {
      console.log("FIND MESSAGES", e);
    }
  };

  public clear = () => {
    try {
      this.realm.write(() => this.realm.deleteAll());
    } catch (e) {
      console.warn(e);
    }
  };
}
