import {ObjectSchema} from 'realm';

export const GoalSchema: ObjectSchema = {
  name: 'Goal',
  primaryKey: 'id',
  properties: {
    id: 'string',
    title: 'string',
    days_for_complete: 'int',
    created_at: "date",
    ends_at: "date",
  }
};
