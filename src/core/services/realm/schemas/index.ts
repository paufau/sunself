import {GoalSchema} from "./GoalSchema";
import {GoalCommentSchema} from "./GoalCommentSchema";
import {PromptConfigSchema} from "./PromptConfigSchema";
import {PromptSchema} from "./PromptSchema";
import {NotificationSchema} from "./NotificationSchema";
import {UserActivitySchema} from "./UserActivitySchema";
import {NotificationConfigSchema} from "./NotificationConfigSchema";


const Schemas = [
  GoalSchema,
  GoalCommentSchema,
  PromptConfigSchema,
  PromptSchema,
  NotificationSchema,
  UserActivitySchema,
  NotificationConfigSchema,
]

export default Schemas;
