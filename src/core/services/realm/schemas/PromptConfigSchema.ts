import {ObjectSchema} from 'realm';

export const PromptConfigSchema: ObjectSchema = {
  name: 'PromptConfig',
  primaryKey: 'id',
  properties: {
    id: 'string',
    is_prompts_enabled: {type: "bool", default: true},
  }
};
