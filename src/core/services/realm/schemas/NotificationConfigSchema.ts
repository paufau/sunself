import {ObjectSchema} from 'realm';

export const NotificationConfigSchema: ObjectSchema = {
  name: 'NotificationConfig',
  primaryKey: 'id',
  properties: {
    id: 'int',
    is_notifications_enabled: {type: "bool", default: true},
  }
};
