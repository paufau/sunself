import {ObjectSchema} from 'realm';

export const PromptSchema: ObjectSchema = {
  name: 'Prompt',
  primaryKey: 'tag',
  properties: {
    tag: "string",
    is_shown: {type: "bool", default: false},
  }
};
