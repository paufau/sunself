import {ObjectSchema} from 'realm';

export const NotificationSchema: ObjectSchema = {
  name: 'Notification',
  primaryKey: 'id',
  properties: {
    id: "string",
    type: "string",
    send_at: "date"
  }
};
