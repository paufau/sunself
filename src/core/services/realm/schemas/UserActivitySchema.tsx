import {ObjectSchema} from 'realm';

export const UserActivitySchema: ObjectSchema = {
  name: 'UserActivity',
  primaryKey: 'id',
  properties: {
    id: "int",
    activity: "date"
  }
};
