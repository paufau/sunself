import {ObjectSchema} from 'realm';

export const GoalCommentSchema: ObjectSchema = {
  name: 'GoalComment',
  primaryKey: 'id',
  properties: {
    id: 'string',
    goal_id: "string",
    created_at: "date",
    commented_date: "date",
    text: 'string',
  }
};
