import * as React from "react";
import {
  Animated,
  BackHandler,
  NativeEventSubscription,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from "react-native";
import theme from "../../style/theme";
import PromptStore from "../../stores/PromptStore";
import {inject, observer} from "mobx-react";
import PromptMessage from "./PromptMessage";
import PromptService from "./PromptService";
import {toJS} from "mobx";
import {LocalizationContext, localizedData} from "../../localization/useLocalization";

interface IProps {
  prompt?: PromptStore;
}

@inject('prompt')
@observer
export default class PromptView extends React.Component<IProps, any>{
  private backHandlerSubscription: NativeEventSubscription | null = null;
  private animation = new Animated.Value(0);

  componentWillUnmount() {
    this.backHandlerSubscription && this.backHandlerSubscription.remove();
  }

  private onPressDone = () => {
    try {
      PromptService.markPromptAsShown(toJS(this.props.prompt!.shownPrompt!.tag));
      this.props.prompt?.setShownPrompt(undefined);
    } catch (e) {
      console.warn(e);
    }
  }

  private onPressDisablePrompts = () => {
    this.onPressDone();
    PromptService.setPromptsEnabled(false);
  }

  private onPressOutside = () => {
    if (this.props.prompt!.shownPrompt) {
      this.onPressDone();
    } else {
      const {shownPromptViewParams} = this.props.prompt!;
      shownPromptViewParams && shownPromptViewParams.onPressOutside && shownPromptViewParams.onPressOutside();
      this.props.prompt?.setShownPromptViewParams(undefined);
    }
  }

  private onPressOption = () => {
    if (this.props.prompt!.shownPrompt) {
      this.props.prompt?.setShownPrompt(undefined);
    } else {
      this.props.prompt?.setShownPromptViewParams(undefined);
    }
  };

  private prevPrompt: any;
  private prevPromptViewParams: any;

  render() {
    const {shownPromptViewParams, shownPrompt} = this.props.prompt!;
    if (shownPrompt || shownPromptViewParams) {
      Animated.timing(this.animation, {
        toValue: 1,
        duration: 300,
        useNativeDriver: true
      }).start();

      this.prevPrompt = shownPrompt;
      this.prevPromptViewParams = shownPromptViewParams;

      this.backHandlerSubscription = BackHandler.addEventListener("hardwareBackPress", () => {
        this.backHandlerSubscription && this.backHandlerSubscription.remove();
        const {shownPromptViewParams, shownPrompt} = this.props.prompt!;
        if (shownPrompt || shownPromptViewParams) {
          this.onPressOutside();
          return true;
        }
        return false;
      })
    } else {
      Animated.timing(this.animation, {
        toValue: 0,
        duration: 300,
        useNativeDriver: true
      }).start(() => {
        this.prevPrompt = null;
        this.prevPromptViewParams = null;
        this.forceUpdate();
      });

      this.backHandlerSubscription && this.backHandlerSubscription.remove();
    }

    const prompt = shownPrompt || this.prevPrompt;
    const promptParams = shownPromptViewParams || this.prevPromptViewParams;

    return (
      <View style={{
        flex: 1,
        zIndex: 1,
        position: 'relative',
      }}>
        {prompt || promptParams ? (
          <React.Fragment>
            <Animated.View style={{
              position: 'absolute',
              top: '10%',
              left: 0,
              right: 0,
              zIndex: 2,
              opacity: this.animation
            }}>
              {promptParams ? (
                <PromptMessage
                  params={promptParams}
                  onPressOption={this.onPressOption}
                />
              ) : (
                <LocalizationContext.Consumer>
                  {localizedData => {
                    // @ts-ignore
                    const title = localizedData.prompts[prompt!.tag];
                    return (
                      <PromptMessage
                        onPressOption={this.onPressOption}
                        params={{
                        title,
                        options: [
                          {
                            title: localizedData.prompts.disable_prompts,
                            color: theme.rgba(theme.white, 0.4),
                            onPress: this.onPressDisablePrompts
                          },
                          {
                            title: localizedData.prompts.understand,
                            color: theme.green,
                            onPress: this.onPressDone
                          },
                        ]
                      }} />
                    )
                  }}
                </LocalizationContext.Consumer>
              )}
            </Animated.View>
            <Animated.View style={{
              position: 'absolute',
              zIndex: 1,
              width: '100%',
              height: '100%',
              opacity: this.animation,
            }}>
              <TouchableOpacity onPress={this.onPressOutside} activeOpacity={1} style={{
                zIndex: 1,
                width: '100%',
                height: '100%',
                backgroundColor: theme.rgba(theme.dark, 0.5),
              }}>
                <React.Fragment />
              </TouchableOpacity>
            </Animated.View>
          </React.Fragment>
        ) : null}
        {this.props.children}
      </View>
    )
  }
}
