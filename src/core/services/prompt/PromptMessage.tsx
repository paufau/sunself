import * as React from "react";
import {TouchableOpacity, View} from "react-native";
import {IPromptViewParams} from "./PromptService";
import theme from "../../style/theme";
import Text from "../../components/Text";

interface IProps {
  params: IPromptViewParams;
  onPressOption: () => void;
}

const PromptMessage: React.FunctionComponent<IProps> = props => {
  const opt1 = props.params.options ? props.params.options[0] : undefined;
  const opt2 = props.params.options ? props.params.options[1] : undefined;
  return (
    <View style={{
      width: '100%',
      backgroundColor: theme.pink,
    }}>
      <Text style={{
        marginTop: 20,
        marginHorizontal: '10%',
      }}>
        {props.params.title}
      </Text>
      <View style={{
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}>
        {opt1 ? (
          <TouchableOpacity
            onPress={() => {
              opt1.onPress();
              props.onPressOption();
            }}
            style={{
              paddingVertical: 20,
              paddingLeft: '10%',
            }}
          >
            <Text style={{
              color: opt1.color
            }}>
              {opt1.title}
            </Text>
          </TouchableOpacity>
        ) : null}
        {opt2 ? (
          <TouchableOpacity
            onPress={() => {
              opt2.onPress();
              props.onPressOption();
            }}
            style={{
              paddingVertical: 20,
              paddingRight: '10%',
            }}
          >
            <Text style={{
              color: opt2.color,
              textAlign: "right",
            }}>
              {opt2.title}
            </Text>
          </TouchableOpacity>
        ) : null}
      </View>
    </View>
  );
}

export default PromptMessage
