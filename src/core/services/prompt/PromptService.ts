import RealmService from "../realm/RealmService";
import {PromptConfigSchema} from "../realm/schemas/PromptConfigSchema";
import {PromptSchema} from "../realm/schemas/PromptSchema";
import {Stores} from "../../stores/Provider";
import Observable from "../../components/Observable";

export interface IPrompt {
  tag: string;
  is_shown?: boolean;
}

export interface IPromptConfig {
  id: string;
  is_prompts_enabled: boolean;
}

export interface ICallPromptListeners {
  onReject?: () => void;
}

export interface IPromptViewParams {
  title: string;
  options: {
    title: string;
    color: string;
    onPress: () => void;
  }[];
  onPressOutside?: () => void;
}

export default class PromptService {
  public static isPromptsEnabledStream = new Observable<boolean>(false);

  public static init = () => {
    const realm = RealmService.getInstance();

    const config = realm.find<IPromptConfig>({
      schemaName: PromptConfigSchema.name,
      predicate: 'id = "0"'
    })[0];
    if (!config) {
      realm.save<IPromptConfig>(PromptConfigSchema.name, {id: "0", is_prompts_enabled: true});
      PromptService.isPromptsEnabledStream.next(true);
    } else {
      PromptService.isPromptsEnabledStream.next(config.is_prompts_enabled);
    }
  }

  public static callPrompt = (tag: string, listeners?: ICallPromptListeners) => {
    if (PromptService.isPromptsEnabledStream.value) {
      try {
        const realm = RealmService.getInstance();
        const prompt = realm.find<IPrompt>({
          schemaName: PromptSchema.name,
          predicate: `tag = "${tag}"`,
        })[0]

        if (!prompt) {
          Stores.prompt.setShownPrompt({
            tag,
            is_shown: false,
          });
        } else if (listeners && listeners.onReject) {
          listeners.onReject();
        }

      } catch (e) {
        console.warn(e);
      }
    } else if (listeners && listeners.onReject){
      listeners.onReject();
    }
  };

  public static markPromptAsShown = (tag: string) => {
    try {
      const realm = RealmService.getInstance();
      realm.save(PromptSchema.name, {
        tag,
        is_shown: true,
      });
    } catch (e) {
      console.warn(e);
    }
  }

  public static setPromptsEnabled = (enabled: boolean) => {
    PromptService.isPromptsEnabledStream.next(enabled);
    const realm = RealmService.getInstance();
    realm.save<IPromptConfig>(PromptConfigSchema.name, {
      id: "0",
      is_prompts_enabled: enabled,
    })
  };

  public static callPromptView = (params: IPromptViewParams) => {
    Stores.prompt.setShownPromptViewParams(params);
  }
}
