
export enum NotificationType {
  NO_ACTIVE_GOALS = 'no_active_goals',
  NO_COMMENT = 'no_comment',
  FLOWER_GROWS_UP = 'flower_grows_up',
  GOAL_LAST_DAY = 'goal_last_day',
  NO_ACTIVITY_YESTERDAY = 'no_activity_yesterday'
}
