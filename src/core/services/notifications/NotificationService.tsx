import {NotificationType} from "./NotificationType";
import PushNotification from "react-native-push-notification";
import RealmService from "../realm/RealmService";
import {NotificationSchema} from "../realm/schemas/NotificationSchema";
import moment from "moment";
import generateID from "../IDGenerator";
import {formatDate, getFormattedNow, getFormattedToday} from "../DateService";
import {localizedData} from "../../localization/useLocalization";
import {NotificationConfigSchema} from "../realm/schemas/NotificationConfigSchema";

export interface INotification {
  id: string;
  type: NotificationType;
  send_at: string;
}

export interface INotificationConfig {
  id: number,
  is_notifications_enabled: boolean,
}

export default class NotificationService {
  private sendNotificationMessage = (type: NotificationType, params: { message: string }) => {
    PushNotification.localNotification({
      title: "Sunself",
      vibrate: false,
      ...params,
    });

    const realm = RealmService.getInstance();
    realm.save<INotification>(NotificationSchema.name, {
      id: generateID(),
      type,
      send_at: getFormattedNow()
    })
  };

  public static isNotificationEnabled: boolean = true;

  public static setNotificationsEnabled = (enabled: boolean) => {
    NotificationService.isNotificationEnabled = enabled;
    const realm = RealmService.getInstance();
    realm.save<INotificationConfig>(NotificationConfigSchema.name, {
      id: 0,
      is_notifications_enabled: enabled,
    })
  };

  public static init = () => {
    const realm = RealmService.getInstance();
    const notificationsConfig = realm.find<INotificationConfig>({
      schemaName: NotificationConfigSchema.name,
      predicate: 'id = 0'
    })[0];

    if (!notificationsConfig) {
      NotificationService.setNotificationsEnabled(true);
    } else {
      NotificationService.isNotificationEnabled = notificationsConfig.is_notifications_enabled;
    }
  };

  public static processPush = (type: NotificationType) => {
    const ns = new NotificationService();
    const pushPeriod = ns.getPeriod(type);
    if (pushPeriod && ns.isTimeAvailable(pushPeriod) && !ns.hasSendNotificationAtPeriod({type, ...pushPeriod})) {
      ns.sendNotificationMessage(type, {message: localizedData.push_notifications[type]})
    }
  };

  private getPeriod = (type: NotificationType): { start: Date, end: Date } | void => {
    const now = moment(new Date());
    switch (type) {
      case NotificationType.NO_ACTIVE_GOALS:
        if (this.isTimeAvailable({
          start: moment(`14:00 ${now.format('DD.MM.YYYY')}`, 'HH:mm DD.MM.YYYY').toDate(),
          end: moment(`16:00 ${now.format('DD.MM.YYYY')}`, 'HH:mm DD.MM.YYYY').toDate(),
        })) {
          return {
            start: moment(new Date()).startOf("week").toDate(),
            end: moment(new Date()).endOf("week").toDate()
          };
        }
      case NotificationType.FLOWER_GROWS_UP:
        return {
          start: moment(`10:00 ${now.format('DD.MM.YYYY')}`, 'HH:mm DD.MM.YYYY').toDate(),
          end: moment(`12:00 ${now.format('DD.MM.YYYY')}`, 'HH:mm DD.MM.YYYY').toDate(),
        }
      case NotificationType.GOAL_LAST_DAY:
        return {
          start: moment(`14:00 ${now.format('DD.MM.YYYY')}`, 'HH:mm DD.MM.YYYY').toDate(),
          end: moment(`16:00 ${now.format('DD.MM.YYYY')}`, 'HH:mm DD.MM.YYYY').toDate(),
        }
      case NotificationType.NO_COMMENT:
        return {
          start: moment(`18:00 ${now.format('DD.MM.YYYY')}`, 'HH:mm DD.MM.YYYY').toDate(),
          end: moment(`20:00 ${now.format('DD.MM.YYYY')}`, 'HH:mm DD.MM.YYYY').toDate(),
        }
    }
  };

  private isTimeAvailable = (period: { start: Date, end: Date }): boolean => {
    const now = moment(new Date());
    return moment(period.start).unix() < now.unix() && moment(period.end).unix() > now.unix();
  };

  private hasSendNotificationAtPeriod = (params: {start: Date, end: Date, type: NotificationType}) => {
    const realm = RealmService.getInstance();
    const foundNotification = realm.find<INotification>({
      schemaName: NotificationSchema.name,
      predicate: `type = "${params.type}" and send_at > ${formatDate(params.start)} and send_at < ${formatDate(params.end)}`
    })

    return foundNotification.length > 0;
  }
}
