import uuid from 'react-native-uuid';

export default function generateID() {
  return uuid.v1();
}
