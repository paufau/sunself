import * as React from "react";
import {Dimensions, KeyboardAvoidingView, Platform, ScrollView, TextInput, View} from "react-native";
import theme from "../../core/style/theme";
import {StackNavigationProp} from "@react-navigation/stack";
import {RouteProp} from "@react-navigation/native";
import {LocalizationContext, localizedData} from "../../core/localization/useLocalization";
import Fonts from "../../core/style/Fonts";
import RadialGradientView from "../../core/components/RadialGradientView";
import {inject, observer} from "mobx-react";
import {Routes} from "../../core/navigation/Routes";
import {RootStackNavigationParams} from "../../core/navigation/NavigationContainer";
import AddGoalHeader from "../AddGoalScreen/modules/AddGoalHeader";
import moment from "moment";
import CommentStore from "../../core/stores/CommentStore";
import getArrayOfDates from "../../core/services/DateService";
import ToastService from "../../core/services/toast/ToastService";

interface IProps {
  navigation: StackNavigationProp<RootStackNavigationParams, Routes.AddCommentScreen>;
  route: RouteProp<RootStackNavigationParams, Routes.AddCommentScreen>;
  comments: CommentStore;
}

interface IState {
  comment_text: string;
}

@inject('comments')
@observer
export default class AddCommentScreen extends React.Component<IProps, IState>{
  state = {
    comment_text: '',
  }

  private datesCalendar: Date[];
  private selectedDate: Date;

  constructor(props: IProps) {
    super(props);
    const {goal} = props.route.params;
    this.datesCalendar = getArrayOfDates(
      moment(goal.created_at).toDate(),
      moment(goal.created_at).add(goal.days_for_complete - 1, 'd').toDate()
    );
    this.selectedDate = this.datesCalendar.find(d => moment(d).isSame(new Date(), "d"))!
  }

  private onPressCancel = () => {
    this.props.navigation.goBack();
  }

  private isOnPressDoneProcessing = false;
  private onPressDone = () => {
    if (!this.isOnPressDoneProcessing) {
      this.isOnPressDoneProcessing = true;
      const {goal} = this.props.route.params;
      this.props.comments.createComment(
        goal.id,
        moment(this.selectedDate).format('YYYY-MM-DD[T]HH:mm:ss'),
        this.state.comment_text
      ).then(comment => {
        this.props.navigation.goBack();
        ToastService.open(localizedData.toast.comment_created);
      }).catch(() => {
        this.isOnPressDoneProcessing = false;
      })
    }
  }

  render() {
    return (
      <View style={{
        flex: 1,
        backgroundColor: theme.white
      }}>
        <KeyboardAvoidingView
          enabled
          behavior={Platform.OS == "ios" ? "padding" : "height"}
          style={{flex: 1}}
        >
          <ScrollView style={{flex: 1}}>
            <AddGoalHeader
              onPressDone={this.onPressDone}
              onPressCancel={this.onPressCancel}
              isDoneEnabled={this.state.comment_text.trim().length > 0}
            />
            <View style={{
              alignItems: 'center',
              width: Dimensions.get("window").width,
            }}>
              <RadialGradientView width={Dimensions.get("window").width}/>
{/*              <CalendarList
                data={this.datesCalendar}
                initialIndex={this.datesCalendar.findIndex(d => moment(d).isSame(new Date(), "d"))}
                onSelectDay={(index: number) => {
                  this.selectedDate = this.datesCalendar[index];
                }}
              />
              <RadialGradientView width={Dimensions.get("window").width}/>*/}
            </View>
            <LocalizationContext.Consumer>
              {localizedData => (
                <TextInput
                  autoFocus
                  scrollEnabled={false}
                  value={this.state.comment_text}
                  onChangeText={(text) => {
                    this.setState({comment_text: text})
                  }}
                  style={{
                    flex: 1,
                    marginTop: 30,
                    marginBottom: 20,
                    paddingHorizontal: '10%',
                    borderWidth: 0,
                    backgroundColor: 'transparent',
                    textAlignVertical: 'top',
                    fontSize: 14,
                    fontFamily: Fonts.TenorSans,
                    color: theme.pink_dark,
                  }}
                  multiline
                  placeholderTextColor={theme.rgba(theme.pink_dark, 0.2)}
                  placeholder={localizedData.add_comment_screen.enter_comment}
                  selectionColor={theme.rgba(theme.green, 0.5)}
                />
              )}
            </LocalizationContext.Consumer>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}
