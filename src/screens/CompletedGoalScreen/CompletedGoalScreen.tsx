import * as React from 'react';
import {Dimensions, TouchableOpacity, View} from "react-native";
import theme from "../../core/style/theme";
import Header from "../../core/components/Header";
import {LocalizationContext, localizedData} from "../../core/localization/useLocalization";
import {StackNavigationProp} from "@react-navigation/stack";
import {DairyStackNavigationParams, RootStackNavigationParams} from "../../core/navigation/NavigationContainer";
import {Routes} from "../../core/navigation/Routes";
import {RouteProp} from "@react-navigation/native";
import GoalsList from "../HomeScreen/modules/GoalsList";
import GoalStore, {IGoal} from "../../core/stores/GoalStore";
import {inject, observer} from "mobx-react";
import AddGoalButton from "../HomeScreen/modules/AddGoalButton";
import Text from "../../core/components/Text";
import PromptService from "../../core/services/prompt/PromptService";

interface IProps {
  navigation: StackNavigationProp<DairyStackNavigationParams & RootStackNavigationParams, Routes.GoalScreen>;
  route: RouteProp<DairyStackNavigationParams & RootStackNavigationParams, Routes.GoalScreen>;
  goals: GoalStore;
}

@inject('goals')
@observer
export default class CompletedGoalScreen extends React.Component<IProps, any>{

  private GoalsListRef: GoalsList | null = null;
  private NavigationFocusUnsubscriber: () => void = () => null;
  private focusedGoalIndex = 0;

  componentDidMount() {
    this.NavigationFocusUnsubscriber = this.props.navigation.addListener("focus", () => {
      const {completed_goals} = this.props.goals;
      this.GoalsListRef && this.GoalsListRef.updateGoalsOnFocus(completed_goals);

      if (!completed_goals || completed_goals.length === 0) {
        this.props.navigation.goBack();
      }
    })
  }

  componentWillUnmount() {
    this.NavigationFocusUnsubscriber();
  }

  private onPressDeleteGoal = (goal: IGoal) => {
    PromptService.callPromptView({
      title: localizedData.goal_screen.delete_goal_modal_message,
      options: [
        {
          title: localizedData.goal_screen.cancel,
          color: theme.rgba(theme.white, 0.4),
          onPress: () => null,
        },
        {
          title: localizedData.goal_screen.delete_goal,
          color: theme.red,
          onPress: () => {
            this.props.goals.deleteGoal(goal.id).then(() => {
              const {completed_goals} = this.props.goals;
              if (completed_goals && completed_goals.length === 0) {
                this.props.navigation.goBack();
              }
            })
          },
        },
      ]
    })
  };

  private onPressGoToComments = (goal: IGoal) => {
    this.props.navigation.navigate(Routes.GoalScreen, {
      goal
    })
  }

  render() {
    const {completed_goals} = this.props.goals;
    const {height, width} = Dimensions.get("window");
    if (!completed_goals) return null;
    return (
      <LocalizationContext.Consumer>
        {localizedData => {
          return (
            <View style={{
              flex: 1,
              backgroundColor: theme.pink_dark
            }}>
              <Header
                title={localizedData.completed_goal_screen.completed}
                leftButton={{
                  title: localizedData.goal_screen.go_back,
                  onPress: this.props.navigation.goBack
                }}
              />
              <GoalsList
                ref={ref => this.GoalsListRef = ref}
                goals={completed_goals || []}
                onFocusedIndexChanged={nextIndex => {
                  this.focusedGoalIndex = nextIndex;
                }}
              />
              <View style={{
                height: height * 0.051,
                marginBottom: height * 0.012,
                flexDirection: 'row',
                alignItems: 'center',
              }}>
                <TouchableOpacity onPress={() => {
                  this.onPressDeleteGoal(completed_goals[this.focusedGoalIndex])
                }} style={{
                  flex: 1,
                  height: '100%',
                  paddingLeft: width * 0.1,
                  justifyContent: 'center',
                }}>
                  <Text style={{
                    fontSize: 10,
                    lineHeight: 12,
                    color: theme.red,
                  }}>{localizedData.goal_screen.delete_goal}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                  this.onPressGoToComments(completed_goals[this.focusedGoalIndex])
                }} style={{
                  flex: 1,
                  height: '100%',
                  paddingRight: width * 0.1,
                  justifyContent: 'center',
                }}>
                  <Text style={{
                    fontSize: 10,
                    lineHeight: 12,
                    color: theme.rgba(theme.white, 0.4),
                    textAlign: 'right',
                  }}>{localizedData.completed_goal_screen.go_to_comments}</Text>
                </TouchableOpacity>
              </View>
            </View>
          )
        }}
      </LocalizationContext.Consumer>
    )
  }
}
