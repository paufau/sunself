import * as React from 'react';
import {Dimensions, View} from "react-native";
import Header from "../../core/components/Header";
import {LocalizationContext, localizedData} from "../../core/localization/useLocalization";
import theme from "../../core/style/theme";
import PromptSelector from "./modules/PromptSelector";
import NotificationSelector from "./modules/NotificationSelector";

interface IProps {

}

interface IState {

}

export default class ProfileScreen extends React.Component<IProps, IState>{
  render() {
    const {width, height} = Dimensions.get("window");
    return (
      <LocalizationContext.Consumer>
        {localizedData => (
          <View style={{
            flex: 1,
            backgroundColor: theme.pink_dark,
          }}>
            <Header
              title={localizedData.tabs.profile}
              fontSize={24}
              lineHeight={28}
            />
            <View style={{
              flex: 1,
              paddingTop: height * 0.036,
              paddingHorizontal: width * 0.1
            }}>
              <NotificationSelector />
              <PromptSelector />
            </View>
          </View>
        )}
      </LocalizationContext.Consumer>
    )
  }
}
