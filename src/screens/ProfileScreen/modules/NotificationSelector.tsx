import * as React from "react";
import PromptService from "../../../core/services/prompt/PromptService";
import {TouchableOpacity, View} from "react-native";
import Text from "../../../core/components/Text";
import {LocalizationContext, localizedData} from "../../../core/localization/useLocalization";
import theme from "../../../core/style/theme";
import NotificationService from "../../../core/services/notifications/NotificationService";

interface IProps {

}

interface IState {
  isEnabled: boolean;
}

export default class NotificationSelector extends React.Component<IProps, IState>{
  constructor(props: IProps) {
    super(props);
    this.state = {
      isEnabled: NotificationService.isNotificationEnabled
    }
  }

  private onPress = () => {
    if (this.state.isEnabled) {
      PromptService.callPromptView({
        title: localizedData.profile_screen.disable_notifications,
        options: [
          {
            title: localizedData.goal_screen.cancel,
            color: theme.rgba(theme.white, 0.4),
            onPress: () => null,
          },
          {
            title: localizedData.profile_screen.turn_off,
            color: theme.red,
            onPress: () => {
              this.setState({isEnabled: false});
              NotificationService.setNotificationsEnabled(false);
            },
          },
        ]
      })
    } else {
      this.setState({isEnabled: true});
      NotificationService.setNotificationsEnabled(true);
    }
  }

  render() {
    const {isEnabled} = this.state;
    return (
      <LocalizationContext.Consumer>
        {localizedData => (
          <TouchableOpacity
            onPress={this.onPress}
            style={{
              height: 40,
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between'
            }}
          >
            <View style={{
              flex: 1,
              height: '100%',
              justifyContent: 'center'
            }}>
              <Text>
                {localizedData.profile_screen.notifications}
              </Text>
            </View>
            <View style={{
              flex: 1,
              height: '100%',
              justifyContent: 'center',
              alignItems: 'flex-end'
            }}>
              <Text style={{
                fontSize: 10,
                lineHeight: 12,
                color: isEnabled ? theme.green : theme.red
              }}>
                {isEnabled ? localizedData.profile_screen.is_enabled : localizedData.profile_screen.is_disabled}
              </Text>
            </View>
          </TouchableOpacity>
        )}
      </LocalizationContext.Consumer>
    );
  }
}
