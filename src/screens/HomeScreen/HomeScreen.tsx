import * as React from 'react';
import {BackHandler, NativeEventSubscription, View} from "react-native";
import PromptService from "../../core/services/prompt/PromptService";
import theme from "../../core/style/theme";
import AddGoalButton from "./modules/AddGoalButton";
import FlowerView from "./modules/FlowerView";
import {Routes} from "../../core/navigation/Routes";
import {StackNavigationProp} from "@react-navigation/stack";
import GoalStore, {IGoal} from "../../core/stores/GoalStore";
import {inject, observer} from "mobx-react";
import SplashScreen from 'react-native-splash-screen'
import CommentStore from "../../core/stores/CommentStore";
import GoalsList from "./modules/GoalsList";
import {toJS} from "mobx";

interface IProps {
  navigation: StackNavigationProp<any>;
  goals: GoalStore;
  comments: CommentStore;
}

interface IState {

}

@inject('goals', 'comments', 'prompt')
@observer
export default class HomeScreen extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    PromptService.callPrompt('welcome');
    SplashScreen.hide();
  }

  private AddGoalButtonRef: AddGoalButton | null = null;
  private GoalsListRef: GoalsList | null = null;
  private NavigationFocusUnsubscriber: () => void = () => null;
  private focusedGoalIndex = 0;

  private checkCommentForGoal = (goal: IGoal) => {
    this.AddGoalButtonRef && this.AddGoalButtonRef.showCommentAddedText(this.props.comments.hasGoalCommentToday(goal.id));
  }

  componentDidMount() {
    this.NavigationFocusUnsubscriber = this.props.navigation.addListener("focus", () => {
      const {user_goals} = this.props.goals;
      this.GoalsListRef && this.GoalsListRef.updateGoalsOnFocus(user_goals);
      if (user_goals && user_goals.length === 0) {
        this.AddGoalButtonRef && this.AddGoalButtonRef.showCommentAddedText(false);
      }
    })
  }

  componentWillUnmount() {
    this.NavigationFocusUnsubscriber();
  }

  render() {
    const {user_goals} = this.props.goals;

    return (
      <View style={{
        flex: 1,
        backgroundColor: theme.pink_dark,
      }}>
        {user_goals && user_goals.length > 0 ? (
          <GoalsList
            ref={ref => this.GoalsListRef = ref}
            goals={toJS(user_goals)}
            onFocusedIndexChanged={nextIndex => {
              this.focusedGoalIndex = nextIndex;
              this.checkCommentForGoal(user_goals[nextIndex]);
            }}
          />
        ) : user_goals ? (
          <FlowerView />
        ) : null}
        <View style={{
          width: '100%',
        }}>
          <AddGoalButton
            ref={ref => this.AddGoalButtonRef = ref}
            primaryItem={user_goals && user_goals.length > 0 ? "comment" : "goal"}
            onPressAddComment={() => {
              this.props.navigation.navigate(Routes.AddCommentScreen, {
                goal: user_goals![this.focusedGoalIndex]
              })
            }}
            onPressAddGoal={() => {
              this.props.navigation.navigate(Routes.AddGoalScreen);
            }}
          />
        </View>
      </View>
    );
  }
};

