import React from "react";
import Svg, {Circle, Path} from "react-native-svg";
import {IIconProps} from "../../../../core/navigation/icons/DairyIcon";

const ArrowIcon = (props: IIconProps) => {
  return (
    <Svg width="30" height="30" viewBox="0 0 30 30" fill="none">
      <Circle opacity="0.5" cx="15" cy="15" r="14.5" stroke="white"/>
      <Path fillRule="evenodd" clipRule="evenodd"
            d="M19.707 15L13.3535 21.3535L13 21.5L18.2928 15L13.3535 8.64646L19.707 15Z" fill="white"/>
    </Svg>
  );
}

export default ArrowIcon;
