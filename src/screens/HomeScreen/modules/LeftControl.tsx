import * as React from "react";
import HidableComponent from "../../../core/components/HidableComponent";
import {View} from "react-native";
import {State, TapGestureHandler} from "react-native-gesture-handler";
import ArrowIcon from "./icons/ArrowIcon";

interface IProps {
  initialHided: boolean;
  onPress: () => void;
}

export default class LeftControl extends React.Component<IProps, any>{
  private hideRef: HidableComponent | null = null;
  private prevTapState: State = State.END;

  public show = () => {
    this.hideRef && this.hideRef.show();
  }

  public hide = () => {
    this.hideRef && this.hideRef.hide();
  }

  render() {
    return (
      <HidableComponent mustInitialHide={this.props.initialHided} ref={ref => this.hideRef = ref}>
        <TapGestureHandler onHandlerStateChange={event => {
          if (event.nativeEvent.state === State.END && this.prevTapState === State.ACTIVE) {
            this.props.onPress();
          }
          this.prevTapState = event.nativeEvent.state;
        }}>
          <View style={{
            width: '10%',
            minWidth: 44,
            height: 44,
            zIndex: 1,
            alignItems: 'flex-end',
            justifyContent: 'center',
          }}>
            <View style={{
              transform: [{rotate: '180deg'}]
            }}>
              <ArrowIcon />
            </View>
          </View>
        </TapGestureHandler>
      </HidableComponent>
    )
  }
}
