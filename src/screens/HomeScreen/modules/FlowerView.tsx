import * as React from "react";
import {LocalizationContext} from "../../../core/localization/useLocalization";
import {Dimensions, Image, View} from "react-native";
import Text from "../../../core/components/Text";
import theme from "../../../core/style/theme";
import Flower_0 from "./flowers/Flower_0";
import moment from "moment";

interface IProps {
  days_for_complete?: number;
  days_passed?: number;
  end_date?: string;
}

interface IInterval {
  start: number;
  end: number;
}

const FlowerImages: { [index: number]: any} = {
  0: require('./flowers/flower_01.webp'),
  1: require('./flowers/flower_02.webp'),
  2: require('./flowers/flower_03.webp'),
  3: require('./flowers/flower_04.webp'),
  4: require('./flowers/flower_05.webp'),
  5: require('./flowers/flower_06.webp')
};

function getIntervalIndex(value: number, intervals: IInterval[]) {
  return intervals.findIndex(int => int.start < value && value <= int.end);
}

export function getFlowerIntervalIndex(progress: number) {
  return getIntervalIndex(progress, [
    {start: 0, end: 0.16},
    {start: 0.16, end: 0.32},
    {start: 0.32, end: 0.48},
    {start: 0.48, end: 0.64},
    {start: 0.64, end: 0.8},
    {start: 0.8, end: 1},
  ]);
}

export default class FlowerView extends React.Component<IProps, any>{
  private flowerHeight = Dimensions.get("window").height * 0.4;
  private flowerWidth = 295 * (this.flowerHeight / 350);

  private getProgress = (days_passed: number, days_for_complete: number) => {
    return days_passed / days_for_complete
  };

  private getFlowerImageResource = (days_passed: number, days_for_complete: number) => {


    const progress = this.getProgress(days_passed, days_for_complete);

    if (progress > 1) {
      return FlowerImages[5];
    }

    const flower_index = getFlowerIntervalIndex(progress);

    return FlowerImages[flower_index];
  };

  render() {
    const {height} = Dimensions.get("window");
    const {days_for_complete, end_date, days_passed} = this.props;
    return (
      <LocalizationContext.Consumer>
        {localizedData => (
          <React.Fragment>
            {days_for_complete !== undefined && days_passed !== undefined ? (
              <View style={{
                width: '100%',
                alignItems: 'center',
                justifyContent: 'flex-end',
              }}>
                <Image
                  source={this.getFlowerImageResource(days_passed, days_for_complete)}
                  style={{
                    width: this.flowerWidth,
                    height: this.flowerHeight,
                  }}
                />
                {this.getProgress(days_passed, days_for_complete) > 1 ? (
                  <Text style={{
                    color: theme.green,
                    marginBottom: height * 0.023,
                    marginTop: height * 0.01,
                  }}>
                    {localizedData.completed_goal_screen.complete_date}: <Text
                    style={{color: theme.white}}>{moment(end_date, 'YYYY-MM-DD[T]HH:mm:ss').format('DD.MM.YYYY')}</Text>
                  </Text>
                ) : (
                  <Text style={{
                    color: theme.green,
                    marginBottom: height * 0.023,
                    marginTop: height * 0.01,
                  }}>
                    {localizedData.home_screen.day} {days_passed} <Text
                    style={{color: theme.white}}>/ {days_for_complete}</Text>
                  </Text>
                )}
              </View>
            ) : (
              <View style={{
                flex: 1,
                justifyContent: 'flex-end',
                alignItems: 'center',
                marginBottom: Dimensions.get("window").height * 0.06,
              }}>
                <Text style={{
                  fontSize: 14,
                  lineHeight: 16,
                  color: theme.rgba(theme.white, 0.5),
                }}>
                  {localizedData.home_screen.no_active_goals}
                </Text>
                <Flower_0
                  height={this.flowerHeight}
                  width={this.flowerWidth}
                />
              </View>
            )}
          </React.Fragment>
        )}
      </LocalizationContext.Consumer>
    );
  }
}
