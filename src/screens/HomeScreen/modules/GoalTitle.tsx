import * as React from 'react';
import {Animated, Dimensions, View} from "react-native";
import CommaIcon from "./icons/CommaIcon";
import HidableComponent from "../../../core/components/HidableComponent";
import Text from "../../../core/components/Text";

interface IState {
  title: string;
}

export default class GoalTitle extends React.Component<any, IState>{
  state = {
    title: ''
  }

  private TitleHidableRef: HidableComponent | null = null;

  public changeTitle = (newTitle: string) => {
    this.hide((result) => {
      if (result.finished) {
        this.show(newTitle);
      }
    })
  }

  public hide = (cb?: (result: Animated.EndResult) => void) => {
    this.TitleHidableRef && this.TitleHidableRef.hide(cb);
  }

  public show = (title: string, cb?: () => void) => {
    this.setState({title: title});
    this.TitleHidableRef && this.TitleHidableRef.show(cb);
  }

  render() {
    return (
      <View style={{
        position: 'relative',
        paddingHorizontal: '10%',
        marginBottom: Dimensions.get("window").height * 0.024,
      }}>
        <View style={{
          position: 'absolute',
          top: -(Dimensions.get("window").height * 0.082) * 0.4,
        }}>
          <CommaIcon
            width={81 * (Dimensions.get("window").height * 0.082) / 70}
            height={Dimensions.get("window").height * 0.082}
          />
        </View>
        <View style={{
          position: 'absolute',
          bottom: -(Dimensions.get("window").height * 0.082) * 0.4,
          right: 0,
          transform: [{
            rotate: '180deg',
          }]
        }}>
          <CommaIcon
            width={81 * (Dimensions.get("window").height * 0.082) / 70}
            height={Dimensions.get("window").height * 0.082}
          />
        </View>
        <View style={{
          width: '100%',
          height: (Math.trunc(Dimensions.get("window").height * 0.028) + 3) * 3,
          alignItems: 'center',
          justifyContent: 'center',
          zIndex: 1,
        }}>
          <HidableComponent
            showAnimationConfig={{
              duration: 700
            }}
            hideAnimationConfig={{
              duration: 200
            }}
            ref={ref => this.TitleHidableRef = ref}
          >
            <Text numberOfLines={3} ellipsizeMode={"tail"} style={{
              fontSize: Math.trunc(Dimensions.get("window").height * 0.028),
              lineHeight: Math.trunc(Dimensions.get("window").height * 0.028) + 3,
              textAlign: 'center',
            }}>{this.state.title}</Text>
          </HidableComponent>
        </View>
      </View>
    );
  }
}
