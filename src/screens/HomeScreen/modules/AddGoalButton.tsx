import * as React from "react";
import useLocalization, {LocalizationContext} from "../../../core/localization/useLocalization";
import RadialGradientView from "../../../core/components/RadialGradientView";
import {Dimensions, TouchableOpacity, View} from "react-native";
import Text from "../../../core/components/Text";
import theme from "../../../core/style/theme";
import HidableComponent from "../../../core/components/HidableComponent";

interface IProps {
  primaryItem: 'goal' | 'comment';
  onPressAddGoal: () => void;
  onPressAddComment: () => void;
}

export default class AddGoalButton extends React.Component<IProps, any>{
  private MainButtonRef: HidableComponent | null = null;
  private CommentTextRef: HidableComponent | null = null;
  private isShownComment = false;

  public showCommentAddedText = (show: boolean) => {
    if (show) {
      this.isShownComment = true;
      this.MainButtonRef && this.MainButtonRef.hide();
      this.CommentTextRef && this.CommentTextRef.show();
    } else {
      this.isShownComment = false;
      this.MainButtonRef && this.MainButtonRef.show();
      this.CommentTextRef && this.CommentTextRef.hide();
    }
  }

  private ANIMATION_CONFIG = {
    showAnimationConfig: {
      duration: 700,
    },
    hideAnimationConfig: {
      duration: 700
    }
  }

  render() {
    const {primaryItem, children, onPressAddGoal, onPressAddComment} = this.props;
    return (
      <LocalizationContext.Consumer>
        {localizedData => (
          <React.Fragment>
            <View
              style={{
                width: '100%',
                height: 44,
                alignItems: 'center',
                marginBottom: primaryItem === "goal" ? 40 : 0,
                position: 'relative',
              }}
            >
              <View style={{
                position: 'absolute',
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
                <HidableComponent
                  mustInitialHide={true}
                  ref={ref => this.CommentTextRef = ref}
                  {...this.ANIMATION_CONFIG}
                >
                  <Text style={{
                    color: theme.green,
                    fontSize: 14,
                    lineHeight: 16,
                  }}>
                    {localizedData.home_screen.comment_successfully_added}
                  </Text>
                </HidableComponent>
              </View>
              <HidableComponent
                ref={ref => this.MainButtonRef = ref}
                {...this.ANIMATION_CONFIG}
              >
                <TouchableOpacity
                  onPress={() => {
                    if (!this.isShownComment) {
                      if (primaryItem === "goal") {
                        onPressAddGoal();
                      } else {
                        onPressAddComment();
                      }
                    }
                  }}
                >
                  <View style={{alignItems: 'center'}}>
                    <RadialGradientView width={Dimensions.get("window").width * 0.73}/>
                    <View style={{
                      width: '100%',
                      height: 38,
                      alignItems: 'center',
                      justifyContent: 'center'
                    }}>
                      <Text style={{
                        color: theme.green,
                        fontSize: 14,
                        lineHeight: 16,
                      }}>
                        {primaryItem === "goal" ? localizedData.home_screen.add_goal : localizedData.home_screen.add_comment}
                      </Text>
                    </View>
                    <RadialGradientView width={Dimensions.get("window").width * 0.73}/>
                  </View>
                </TouchableOpacity>
              </HidableComponent>
            </View>
            {primaryItem === "comment" ? (
              <TouchableOpacity style={{
                width: '100%',
                height: 40,
              }} onPress={onPressAddGoal}>
                <View style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                  <Text style={{
                    opacity: 0.4,
                    marginBottom: 10,
                  }}>{localizedData.home_screen.add_goal}</Text>
                </View>
              </TouchableOpacity>
            ) : null}
          </React.Fragment>
        )}
      </LocalizationContext.Consumer>
    );
  }
}
