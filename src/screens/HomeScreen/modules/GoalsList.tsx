import * as React from 'react';
import {IGoal} from "../../../core/stores/GoalStore";
import {Dimensions, View} from "react-native";
import LeftControl from "./LeftControl";
import RightControl from "./RightControl";
import GoalTitle from "./GoalTitle";
import HorizontalCenterSnapList from "../../../core/components/HorizontalCenterSnapList";
import FlowerView from "./FlowerView";
import moment from "moment";
import Container from "../../AddGoalScreen/modules/Container";
import HidableComponent from "../../../core/components/HidableComponent";

interface IProps {
  goals: IGoal[];
  onFocusedIndexChanged: (nextIndex: number) => void;
}

interface IState {

}

export default class GoalsList extends React.Component<IProps, IState>{
  state = {
    s: true
  }

  private LeftControlRef: LeftControl | null = null;
  private ListRef: HorizontalCenterSnapList | null = null;
  private isTitleHided = false;

  private RightControlRef: RightControl | null = null;
  private GoalTitleRef: GoalTitle | null = null;

  private lastShownGoalsLength = 0;
  private focusedGoalIndex = 0;

  private lastGoalsUpdate: IGoal[] = [];
  public updateGoalsOnFocus = (goals?: IGoal[]) => {
    if (goals && goals.length > 0) {
      if (this.lastGoalsUpdate.length !== goals.length) {
        this.setFocusedIndex(0);
      } else {
        this.setFocusedIndex(this.focusedGoalIndex);
      }

      const focusedGoal = goals[this.focusedGoalIndex];
      if (focusedGoal) {
        this.GoalTitleRef && this.GoalTitleRef.show(focusedGoal.title);
      }

      if (this.focusedGoalIndex > 0) {
        this.LeftControlRef && this.LeftControlRef.show();
      } else {
        this.LeftControlRef && this.LeftControlRef.hide();
      }

      if (goals.length > 1) {
        if (this.focusedGoalIndex === goals.length - 1) {
          this.RightControlRef && this.RightControlRef.hide();
        } else {
          this.RightControlRef && this.RightControlRef.show();
        }
      } else {
        this.RightControlRef && this.RightControlRef.hide();
      }

      this.lastGoalsUpdate = goals;
    }
  }

  private setFocusedIndex = (index: number) => {
    this.focusedGoalIndex = index;
    this.props.onFocusedIndexChanged(index);
  }

  render() {
    const {goals} = this.props;
    if (goals) {
      this.lastShownGoalsLength = goals.length;
    }

    return (
      <Container style={{
        justifyContent: 'space-between',
        position: 'relative',
      }}>
        <View style={{
          left: 0,
          bottom: '30%',
          position: 'absolute',
          zIndex: 1,
        }}>
          <LeftControl
            ref={ref => this.LeftControlRef = ref}
            initialHided={true}
            onPress={() => {
              if (this.focusedGoalIndex !== 0) {
                this.isTitleHided = true;
                this.GoalTitleRef && this.GoalTitleRef.hide();
                this.ListRef && this.ListRef.snapToNumberOfItems(-1);
              }
            }}
          />
        </View>
        <View style={{
          right: 0,
          bottom: '30%',
          position: 'absolute',
          zIndex: 1,
        }}>
          <RightControl
            ref={ref => this.RightControlRef = ref}
            initialHided={goals.length <= 1}
            onPress={() => {
              if (goals && this.focusedGoalIndex !== goals.length - 1) {
                this.isTitleHided = true;
                this.GoalTitleRef && this.GoalTitleRef.hide();
                this.ListRef && this.ListRef.snapToNumberOfItems(1);
              }
            }}
          />
        </View>
        <View style={{flex: 1, justifyContent: "flex-end"}}>
          <GoalTitle ref={ref => this.GoalTitleRef = ref}/>
          <HorizontalCenterSnapList
            initialIndex={0}
            onMovementStart={() => {
              this.isTitleHided = true;
              this.GoalTitleRef && this.GoalTitleRef.hide();
            }}
            ref={ref => this.ListRef = ref}
            maxItemsForInertiaScrolling={1}
            data={goals}
            style={{
              width: '100%',
              justifyContent: 'flex-end',
            }}
            onItemCentered={index => {
              if (index > 0) {
                this.LeftControlRef && this.LeftControlRef.show();
              } else {
                this.LeftControlRef && this.LeftControlRef.hide();
              }

              if (goals.length > 0) {
                if (this.isTitleHided) {
                  this.isTitleHided = false;
                  this.GoalTitleRef && this.GoalTitleRef.changeTitle(goals[index].title);
                }
              }

              if (goals.length > 1) {
                if (index === goals.length - 1) {
                  this.RightControlRef && this.RightControlRef.hide();
                } else {
                  this.RightControlRef && this.RightControlRef.show();
                }
              }

              this.setFocusedIndex(index);
            }}
            itemWidth={Dimensions.get("window").width}
            renderItem={(item: IGoal, index: number) => {
              return (
                <View style={{
                  width: Dimensions.get("window").width,
                  justifyContent: 'space-between',
                }}>
                  <FlowerView
                    days_passed={1 + moment(new Date()).startOf('d').diff(moment(item.created_at).startOf("d"), 'days')}
                    days_for_complete={item.days_for_complete}
                    end_date={item.ends_at}
                  />
                </View>
              )
            }}
          />
        </View>
      </Container>
    );
  }
}
