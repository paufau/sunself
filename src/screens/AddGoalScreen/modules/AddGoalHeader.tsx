import * as React from "react";
import {Dimensions, View} from "react-native";
import useLocalization from "../../../core/localization/useLocalization";
import Text from "../../../core/components/Text";
import theme from "../../../core/style/theme";
import {TouchableOpacity} from "react-native-gesture-handler";

interface IProps {
  onPressCancel: () => void;
  onPressDone: () => void;
  isDoneEnabled: boolean;
}

const AddGoalHeader: React.FunctionComponent<IProps> = props => {
  const localization = useLocalization();
  return (
    <View style={{
      marginTop: 40,
    }}>
      <View style={{
        flexDirection: 'row',
      }}>
        <TouchableOpacity style={{
          flex: 1,
          width: Dimensions.get("window").width * 0.4,
          paddingVertical: 20,
          marginLeft: Dimensions.get("window").width * 0.1,
        }} onPress={props.onPressCancel}>
          <Text style={{
            color: theme.rgba(theme.pink_dark, 0.4),
          }}>
            {localization.add_goal_screen.cancel}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          disabled={!props.isDoneEnabled}
          style={{
          paddingVertical: 20,
          flex: 1,
          width: Dimensions.get("window").width * 0.4,
          marginRight: Dimensions.get("window").width * 0.1,
        }} onPress={props.onPressDone}>
          <Text style={{
            color: props.isDoneEnabled ? theme.green :  theme.rgba(theme.pink_dark, 0.4),
            textAlign: "right",
          }}>
            {localization.add_goal_screen.done}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default AddGoalHeader;
