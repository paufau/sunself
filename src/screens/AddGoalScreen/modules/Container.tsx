import React from "react";
import {StyleProp, View, ViewStyle} from "react-native";

interface IProps {
  style?: StyleProp<ViewStyle>
}

const Container: React.FunctionComponent<IProps> = props => {
  return (
    <View style={[{
      flex: 1,
    }, props.style]}>
      {props.children}
    </View>
  )
}

export default Container;
