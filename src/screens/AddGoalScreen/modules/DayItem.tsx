import * as React from 'react';
import {Animated, Image, TouchableOpacity, View} from "react-native";
import Text from "../../../core/components/Text";
import theme from "../../../core/style/theme";

interface IProps {
  initialIsFocused: boolean;
  onPress?: () => void;
  text: string | number;
  width: number;
}

interface IState {
  isFocused: boolean;
}

export default class DayItem extends React.Component<IProps, IState>{
  constructor(props: IProps) {
    super(props);

    this.state = {
      isFocused: props.initialIsFocused,
    }

    this.animation = new Animated.Value(props.initialIsFocused ? 1 : 0);
  }

  private animation: Animated.Value;

  public show = () => {
    this.setState({isFocused: true}, () => {
      Animated.timing(this.animation, {
        toValue: 1,
        duration: 200,
        useNativeDriver: true,
      }).start();
    })
  }

  public hide = () => {
    Animated.timing(this.animation, {
      toValue: 0,
      duration: 200,
      useNativeDriver: true,
    }).start((result) => {
      if (result.finished) {
        this.setState({isFocused: false})
      }
    })
  }

  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress} activeOpacity={0.7} style={{
        width: this.props.width,
        height: 34,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
      }}>
        {this.state.isFocused ? (
          <Animated.View style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
            alignItems: 'center',
            justifyContent: 'center',
            zIndex: 1,
            opacity: this.animation,
          }}>
            <Image
              source={require('./Rectangle.webp')}
              style={{
                marginTop: 6,
                width: 72,
                height: 74,
              }}
            />
          </Animated.View>
        ) : null}
        <Text style={{
          fontSize: 20,
          lineHeight: 26,
          color: theme.pink_dark,
          zIndex: 2,
        }}>{this.props.text}</Text>
      </TouchableOpacity>
    )
  }
}
