import * as React from "react";
import Text from "../../../core/components/Text";
import useLocalization from "../../../core/localization/useLocalization";
import theme from "../../../core/style/theme";

interface IProps {

}

const DaysForCompleteText: React.FunctionComponent<IProps> = props => {
  const localization = useLocalization();
  return (
    <Text style={{
      fontSize: 10,
      lineHeight: 12,
      color: theme.rgba(theme.pink_dark, 0.4),
      marginBottom: 10,
    }}>{localization.add_goal_screen.days_for_complete}</Text>
  )
};

export default DaysForCompleteText;
