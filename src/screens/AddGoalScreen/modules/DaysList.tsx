import * as React from "react";
import DayItem from "./DayItem";
import HorizontalCenterSnapList from "../../../core/components/HorizontalCenterSnapList";

interface IProps {
  onSelectDay: (days: number) => void;
  initialIndex?: number;
}

interface IState {

}

export default class DaysList extends React.Component<IProps, IState>{
  private DAYS_TO_RENDER = 62;
  private INITIAL_INDEX = 29;
  private ITEM_WIDTH = 52;

  private DayItemRefs: any = {};
  private focusedItemIndex = this.INITIAL_INDEX;

  constructor(props: IProps) {
    super(props);
    if (props.initialIndex !== undefined) {
      this.INITIAL_INDEX = props.initialIndex;
      this.focusedItemIndex = this.INITIAL_INDEX;
    }
  }

  private activateDayAnimation = (index: number) => {
    if (index !== this.focusedItemIndex) {
      this.props.onSelectDay(index + 1);
      this.DayItemRefs[`${this.focusedItemIndex}`].hide();
      this.DayItemRefs[`${index}`].show();
      this.focusedItemIndex = index;
    }
  }

  render() {
    return (
      <HorizontalCenterSnapList
        initialIndex={this.INITIAL_INDEX}
        data={Array.from(Array(this.DAYS_TO_RENDER).keys())}
        style={{
          marginTop: 20,
          marginBottom: 10,
          width: '100%',
          height: 54,
        }}
        itemWidth={this.ITEM_WIDTH}
        onItemCentered={(index) => {
          this.activateDayAnimation(index);
        }}
        renderItem={((item, index) => {
          return (
            <DayItem
              key={index}
              ref={ref => this.DayItemRefs[`${index}`] = ref}
              text={index + 1}
              width={this.ITEM_WIDTH}
              initialIsFocused={this.INITIAL_INDEX === index}
            />
          );
        })}
      />
    );
  }
}
