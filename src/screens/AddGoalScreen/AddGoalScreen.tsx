import * as React from "react";
import {Dimensions, KeyboardAvoidingView, Platform, ScrollView, TextInput, View} from "react-native";
import theme from "../../core/style/theme";
import {StackNavigationProp} from "@react-navigation/stack";
import {LocalizationContext, localizedData} from "../../core/localization/useLocalization";
import Fonts from "../../core/style/Fonts";
import AddGoalHeader from "./modules/AddGoalHeader";
import RadialGradientView from "../../core/components/RadialGradientView";
import DaysList from "./modules/DaysList";
import DaysForCompleteText from "./modules/DaysForCompleteText";
import GoalStore, {IGoal} from "../../core/stores/GoalStore";
import {inject, observer} from "mobx-react";
import {RootStackNavigationParams} from "../../core/navigation/NavigationContainer";
import {RouteProp} from "@react-navigation/native";
import {Routes} from "../../core/navigation/Routes";
import ToastService from "../../core/services/toast/ToastService";
import PromptService from "../../core/services/prompt/PromptService";

interface IProps {
  navigation: StackNavigationProp<RootStackNavigationParams>;
  route: RouteProp<RootStackNavigationParams, Routes.AddGoalScreen>;
  goals: GoalStore;
}

interface IState {
  goal_text: string;
}

@inject('goals')
@observer
export default class AddGoalScreen extends React.Component<IProps, IState>{
  state = {
    goal_text: '',
  }

  private days_for_complete = 30;

  constructor(props: IProps) {
    super(props);

    const {goal} = this.props.route.params ? this.props.route.params : {goal: undefined};
    if (goal) {
      this.days_for_complete = goal.days_for_complete;
      this.state = {
        goal_text: goal.title
      }
    }
  }

  private onPressCancel = () => {
    this.props.navigation.goBack();
  }

  private isOnPressDoneProcessing = false;
  private onPressDone = () => {
    if (!this.isOnPressDoneProcessing) {
      this.isOnPressDoneProcessing = true;
      const {goal_text} = this.state;
      const {goal} = this.props.route.params ? this.props.route.params : {goal: undefined};
      if (goal_text.length > 0) {
        if (goal) {
          this.props.goals.editGoal({
            ...goal,
            title: goal_text,
            days_for_complete: this.days_for_complete,
          }).then(() => {
            this.props.navigation.goBack();
          }).catch(() => {
            this.isOnPressDoneProcessing = false;
          })
        } else {
          this.props.goals.createNewGoal(goal_text, this.days_for_complete)
            .then((new_goal: IGoal) => {
              this.props.navigation.goBack();
              PromptService.callPrompt("first_goal", {
                onReject: () => {
                  ToastService.open(localizedData.toast.goal_created);
                }
              })
            })
            .catch(() => {
              this.isOnPressDoneProcessing = false;
            })
        }
      } else {
        this.isOnPressDoneProcessing = false;
      }
    }
  }

  render() {
    return (
      <View style={{
        flex: 1,
        backgroundColor: theme.white
      }}>
        <KeyboardAvoidingView
          enabled
          behavior={Platform.OS == "ios" ? "padding" : "height"}
          style={{flex: 1}}
        >
          <ScrollView style={{flex: 1}}>
            <AddGoalHeader
              onPressDone={this.onPressDone}
              onPressCancel={this.onPressCancel}
              isDoneEnabled={this.state.goal_text.trim().length > 0}
            />
            <View style={{
              alignItems: 'center',
              width: Dimensions.get("window").width,
            }}>
              <RadialGradientView width={Dimensions.get("window").width}/>
              <DaysList
                onSelectDay={days => {
                  this.days_for_complete = days;
                }}
                initialIndex={this.days_for_complete - 1}
              />
              <DaysForCompleteText/>
              <RadialGradientView width={Dimensions.get("window").width}/>
            </View>
            <LocalizationContext.Consumer>
              {localizedData => (
                <TextInput
                  autoFocus
                  scrollEnabled={false}
                  value={this.state.goal_text}
                  onChangeText={(text) => {
                    this.setState({goal_text: text})
                  }}
                  style={{
                    flex: 1,
                    marginTop: 30,
                    marginBottom: 20,
                    paddingHorizontal: '10%',
                    borderWidth: 0,
                    backgroundColor: 'transparent',
                    textAlignVertical: 'top',
                    fontSize: 14,
                    fontFamily: Fonts.TenorSans,
                    color: theme.pink_dark,
                  }}
                  multiline
                  placeholderTextColor={theme.rgba(theme.pink_dark, 0.2)}
                  placeholder={localizedData.add_goal_screen.what_goal_do_you_want_to_set}
                  selectionColor={theme.rgba(theme.green, 0.5)}
                />
              )}
            </LocalizationContext.Consumer>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}
