import * as React from "react";
import {IGoalComment} from "../../../core/stores/CommentStore";
import {Dimensions, View} from "react-native";
import theme from "../../../core/style/theme";
import Text from "../../../core/components/Text";
import moment from "moment";

interface IProps {
  comment: IGoalComment;
}

export default class CommentItem extends React.Component<IProps, any>{
  render() {
    const {width} = Dimensions.get("window");
    return (
      <View style={{
        backgroundColor: theme.pink,
        paddingVertical: 16,
        paddingHorizontal: width * 0.05,
        marginTop: 5,
        flexDirection: 'row',
        alignItems: 'flex-start',
        minHeight: 40,
      }}>
        <Text style={{
          color: theme.green,
          width: 40 + (width * 0.05),
        }}>{moment(this.props.comment.commented_date, 'YYYY-MM-DD[T]HH:mm:ss').format('DD MMM')}</Text>
        <Text>{this.props.comment.text}</Text>
      </View>
    )
  }
}
