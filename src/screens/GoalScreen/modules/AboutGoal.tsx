import * as React from "react";
import {Dimensions, TouchableOpacity, View} from "react-native";
import Text from "../../../core/components/Text";
import RadialGradientView from "../../../core/components/RadialGradientView";
import useLocalization from "../../../core/localization/useLocalization";
import theme from "../../../core/style/theme";

interface IProps {
  title: string;
  onPressDelete: () => void;
  onPressEdit?: () => void;
}

const AboutGoal: React.FunctionComponent<IProps> = props => {
  const {height, width} = Dimensions.get("window");
  const localizedData = useLocalization();

  return (
    <React.Fragment>
      <View style={{
        width: ' 100%',
      }}>
        <Text style={{
          fontSize: 14,
          lineHeight: 16,
          paddingBottom: height * 0.03,
          marginTop: height * 0.036,
          marginHorizontal: width * 0.1,
        }}>{props.title}</Text>
        <View style={{
          flexDirection: 'row',
          justifyContent: "space-between"
        }}>
          <TouchableOpacity
            onPress={props.onPressDelete}
            style={{
              paddingTop: height * 0.03,
              paddingLeft: width * 0.1,
              paddingBottom: height * 0.036,
              flex: 1,
            }}
          >
            <Text style={{
              fontSize: 10,
              lineHeight: 12,
              color: theme.red
            }}>{localizedData.goal_screen.delete_goal}</Text>
          </TouchableOpacity>
          {props.onPressEdit ? (
            <TouchableOpacity
              onPress={props.onPressEdit}
              style={{
                paddingTop: height * 0.03,
                paddingRight: width * 0.1,
                paddingBottom: height * 0.036,
                flex: 1,
              }}
            >
              <Text style={{
                fontSize: 10,
                lineHeight: 12,
                color: theme.rgba(theme.white, 0.4),
                textAlign: 'right'
              }}>{localizedData.goal_screen.edit_goal}</Text>
            </TouchableOpacity>
          ) : null}
        </View>
      </View>
      <RadialGradientView width={width}/>
    </React.Fragment>
  );
};

export default AboutGoal;
