import * as React from "react";
import {LocalizationContext} from "../../../core/localization/useLocalization";
import {Dimensions, TouchableOpacity, View} from "react-native";
import RadialGradientView from "../../../core/components/RadialGradientView";
import Text from "../../../core/components/Text";
import theme from "../../../core/style/theme";

interface IProps {
  onPressAddComment: () => void;
  days_passed: number;
  days_for_complete: number;
}

export default class AddCommentButton extends React.PureComponent<IProps>{
  render() {
    const {onPressAddComment, days_for_complete, days_passed} = this.props;
    return (
      <LocalizationContext.Consumer>
        {localizedData => (
          <React.Fragment>
            <TouchableOpacity onPress={onPressAddComment} style={{
              width: '100%',
              height: 44,
              alignItems: 'center',
            }}>
              <View style={{alignItems: 'center'}}>
                <RadialGradientView width={Dimensions.get("window").width * 0.73}/>
                <View style={{
                  width: '100%',
                  height: 38,
                  alignItems: 'center',
                  justifyContent: 'center'
                }}>
                  <Text style={{
                    color: theme.green,
                    fontSize: 14,
                    lineHeight: 16,
                  }}>
                    {localizedData.home_screen.add_comment}
                  </Text>
                </View>
                <RadialGradientView width={Dimensions.get("window").width * 0.73}/>
              </View>
            </TouchableOpacity>
              <View style={{
                width: '100%',
                height: 40,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
                <Text style={{opacity: 0.4}}>{localizedData.home_screen.day} {days_passed} / {days_for_complete}</Text>
              </View>
          </React.Fragment>
        )}
      </LocalizationContext.Consumer>
    );
  }
}
