import * as React from "react";
import {Dimensions, FlatList, TouchableOpacity, View, Animated, LayoutChangeEvent} from "react-native";
import theme from "../../core/style/theme";
import {StackNavigationProp} from "@react-navigation/stack";
import {DairyStackNavigationParams, RootStackNavigationParams} from "../../core/navigation/NavigationContainer";
import {Routes} from "../../core/navigation/Routes";
import {RouteProp} from "@react-navigation/native";
import AboutGoal from "./modules/AboutGoal";
import CommentStore, {IGoalComment} from "../../core/stores/CommentStore";
import {inject, observer} from "mobx-react";
import CommentItem from "./modules/CommentItem";
import AddCommentButton from "./modules/AddCommentButton";
import moment from "moment";
import ListOverlay from "../DairyScreen/modules/ListOverlay";
import Observable from "../../core/components/Observable";
import Swipeable from "react-native-gesture-handler/Swipeable";
import Text from "../../core/components/Text";
import {LocalizationContext, localizedData} from "../../core/localization/useLocalization";
import PromptService from "../../core/services/prompt/PromptService";
import GoalStore from "../../core/stores/GoalStore";
import Header from "../../core/components/Header";

interface IProps {
  navigation: StackNavigationProp<DairyStackNavigationParams & RootStackNavigationParams, Routes.GoalScreen>;
  route: RouteProp<DairyStackNavigationParams & RootStackNavigationParams, Routes.GoalScreen>;
  comments: CommentStore;
  goals: GoalStore;
}

interface IState {
  comments: IGoalComment[];
}

export const GoalCommentsSubscription = new Observable<undefined | {
  goal_id: string,
  comments: IGoalComment[],
}>(undefined);

@inject('comments', 'goals')
@observer
export default class GoalScreen extends React.Component<IProps, IState> {
  private FooterViewRef: View | null = null;
  private CommentsListenerID: number;
  private lastSwipeableOpenIndex?: number;
  private SwipeableRefs: any = {};
  private TopOverlayY = new Animated.Value(0);
  private TopOverlayOpacity = new Animated.Value(0);
  private TopOverlayHeight = 0;

  constructor(props: IProps) {
    super(props);
    this.state = {
      comments: this.props.comments.getAllGoalComments(props.route.params.goal.id)
    };

    this.CommentsListenerID = GoalCommentsSubscription.subscribe((nextData) => {
      if (nextData && nextData.goal_id === props.route.params.goal.id) {
        this.setState({
          comments: nextData.comments
        })
      }
    })
  }

  componentWillUnmount() {
    GoalCommentsSubscription.describe(this.CommentsListenerID);
  }

  private onPressDeleteComment = (comment: IGoalComment) => {
    this.props.comments.deleteComment(comment);
  };

  private onPressDeleteGoal = () => {
    PromptService.callPromptView({
      title: localizedData.goal_screen.delete_goal_modal_message,
      options: [
        {
          title: localizedData.goal_screen.cancel,
          color: theme.rgba(theme.white, 0.4),
          onPress: () => null,
        },
        {
          title: localizedData.goal_screen.delete_goal,
          color: theme.red,
          onPress: () => {
            this.props.goals.deleteGoal(this.props.route.params.goal.id).then(() => {
              this.props.navigation.goBack();
            })
          },
        },
      ]
    })
  };

  private onPressEdit = () => {
    const {goal: {id}} = this.props.route.params;
    const goal = this.props.goals.allGoals!.find(g => g.id === id);

    if (goal) {
      this.props.navigation.navigate(Routes.AddGoalScreen, {goal});
    }
  }

  render() {
    const {goal: {id}} = this.props.route.params;

    const goal = this.props.goals.allGoals!.find(g => g.id === id);
    if (!goal) return null;

    const isGoalCompleted = moment(goal.ends_at, 'YYYY-MM-DD[T]HH:mm:ss').unix() < moment(new Date()).unix();

    return (
      <LocalizationContext.Consumer>
        {localizedData => {
          return (
            <View style={{
              flex: 1,
              backgroundColor: theme.pink_dark
            }}>
              <Header
                title={localizedData.tabs.dairy}
                leftButton={{
                  title: localizedData.goal_screen.go_back,
                  onPress: this.props.navigation.goBack
                }}
                fontSize={24}
                lineHeight={28}
              />
              <View style={{
                flex: 1,
                overflow: "hidden",
                position: 'relative'
              }}>
                <FlatList
                  scrollEventThrottle={16}
                  onScroll={event => {
                    if (event.nativeEvent.contentOffset.y * 3 <= this.TopOverlayHeight) {
                      this.TopOverlayY.setValue(this.TopOverlayHeight - event.nativeEvent.contentOffset.y * 3)
                    } else {
                      this.TopOverlayY.setValue(0);
                    }
                  }}
                  data={this.state.comments}
                  renderItem={({item, index}) => {
                    return (
                      <Swipeable
                        ref={ref => this.SwipeableRefs[index.toString()] = ref}
                        onSwipeableWillOpen={() => {
                          if (this.lastSwipeableOpenIndex !== undefined && this.lastSwipeableOpenIndex !== index) {
                            const row = this.SwipeableRefs[this.lastSwipeableOpenIndex.toString()];
                            row && row.close();
                          }
                          this.lastSwipeableOpenIndex = index;
                        }}
                        rightThreshold={isGoalCompleted ? 0 : Dimensions.get("window").width / 3}
                        overshootRight={false}
                        renderRightActions={isGoalCompleted ? () => null : (progressAnimatedValue, dragAnimatedValue) => {
                          return (
                            <TouchableOpacity
                              onPress={() => {
                                this.onPressDeleteComment(item);
                              }}
                              style={{
                                flex: 1,
                                maxWidth: Dimensions.get("window").width / 3,
                              }}
                            >
                              <View style={{
                                flex: 1,
                                maxWidth: Dimensions.get("window").width / 3,
                                backgroundColor: theme.red,
                                marginTop: 5,
                                alignItems: 'center',
                                justifyContent: 'center',
                              }}>
                                <Text style={{
                                  fontSize: 10,
                                  lineHeight: 12,
                                }}>{localizedData.goal_screen.delete_comment}</Text>
                              </View>
                            </TouchableOpacity>
                          );
                        }}
                      >
                        <CommentItem comment={item}/>
                      </Swipeable>
                    )
                  }}
                  style={{
                    width: '100%',
                  }}
                  bounces={false}
                  overScrollMode={"never"}
                  showsVerticalScrollIndicator={false}
                  ListHeaderComponent={<View style={{flex: 1, marginBottom: 20}}>
                    <AboutGoal
                      title={goal.title}
                      onPressDelete={this.onPressDeleteGoal}
                      onPressEdit={isGoalCompleted
                        ? undefined
                        : this.onPressEdit
                      }
                    />
                  </View>}
                  ListFooterComponent={<View ref={ref => this.FooterViewRef = ref} style={{height: 20}}/>}
                  ListEmptyComponent={
                    <Text style={{
                      fontSize: 12,
                      lineHeight: 14,
                      color: theme.rgba(theme.white, 0.4),
                      marginTop: Dimensions.get("window").height * 0.036,
                      marginHorizontal: Dimensions.get("window").width * 0.1,
                    }}>
                      {localizedData.goal_screen.has_no_comments}
                    </Text>
                  }
                />
                <View
                  pointerEvents={"none"}
                  onLayout={event => {
                    this.FooterViewRef && this.FooterViewRef.setNativeProps({style: {height: 20 + (event.nativeEvent.layout.height / 3)}})
                  }}
                  style={{
                    position: 'absolute',
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0,
                    justifyContent: 'flex-end',
                  }}
                >
                  <ListOverlay
                    width={Dimensions.get("window").width}
                  />
                </View>
                <View pointerEvents={"none"} style={{
                  position: 'absolute',
                  left: 0,
                  right: 0,
                  top: 0,
                  bottom: 0,
                  justifyContent: 'flex-start',
                }}>
                  <Animated.View
                    onLayout={(event: LayoutChangeEvent) => {
                      this.TopOverlayY.setValue(event.nativeEvent.layout.height);
                      this.TopOverlayHeight = event.nativeEvent.layout.height;
                      this.TopOverlayOpacity.setValue(1);
                    }}
                    style={{
                      opacity: this.TopOverlayOpacity,
                      transform: [
                        {rotate: '180deg'},
                        {translateY: this.TopOverlayY}
                      ],
                    }}
                  >
                    <ListOverlay
                      width={Dimensions.get("window").width}
                    />
                  </Animated.View>
                </View>
              </View>
              {isGoalCompleted ? null : (
                <AddCommentButton
                  onPressAddComment={() => this.props.navigation.navigate(Routes.AddCommentScreen, {
                    goal
                  })}
                  days_passed={1 + moment(new Date()).startOf('d').diff(moment(goal.created_at).startOf("d"), 'days')}
                  days_for_complete={goal.days_for_complete}
                />
              )}
            </View>
          );
        }}
      </LocalizationContext.Consumer>
    );
  }
};
