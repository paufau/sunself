import * as React from 'react';
import {Animated, Dimensions, FlatList, LayoutChangeEvent, View} from "react-native";
import theme from "../../core/style/theme";
import GoalStore, {IGoal} from "../../core/stores/GoalStore";
import {inject, observer} from "mobx-react";
import DairyListItem from "./modules/DairyListItem";
import moment from "moment";
import CommentStore from "../../core/stores/CommentStore";
import DairyAddGoalButton from "./modules/DairyAddGoalButton";
import ListOverlay from "./modules/ListOverlay";
import {StackNavigationProp} from "@react-navigation/stack";
import {Routes} from "../../core/navigation/Routes";
import Header from "../../core/components/Header";
import {LocalizationContext} from "../../core/localization/useLocalization";
import PromptService from "../../core/services/prompt/PromptService";
import FlowerView from "../HomeScreen/modules/FlowerView";
import DairyEmpty from "./modules/DairyEmpty";

interface IProps {
  navigation: StackNavigationProp<any>
  goals: GoalStore;
  comments: CommentStore;
}

interface IState {

}

@inject('goals', 'comments')
@observer
export default class DairyScreen extends React.Component<IProps, IState> {
  private FooterViewRef: View | null = null;

  private TopOverlayY = new Animated.Value(0);
  private TopOverlayOpacity = new Animated.Value(0);
  private TopOverlayHeight = 0;

  constructor(props: IProps) {
    super(props);
    PromptService.callPrompt('dairy_welcome')
  }

  private onPressGoal = (goal: IGoal) => {
    this.props.navigation.navigate(Routes.GoalScreen, {goal});
  }

  render() {
    const {user_goals, completed_goals} = this.props.goals;
    return (
      <LocalizationContext.Consumer>
        {localizedData => {
          return (
            <View style={{
              flex: 1,
              backgroundColor: theme.pink_dark,
            }}>
              <Header
                title={localizedData.tabs.dairy}
                fontSize={24}
                lineHeight={28}
              />
              <View style={{
                flex: 1,
                overflow: 'hidden',
                position: 'relative',
              }}>
                {user_goals && user_goals.length > 0 ? (
                  <FlatList
                    scrollEventThrottle={16}
                    onScroll={event => {
                      if (event.nativeEvent.contentOffset.y * 3 <= this.TopOverlayHeight) {
                        this.TopOverlayY.setValue(this.TopOverlayHeight - event.nativeEvent.contentOffset.y * 3)
                      } else {
                        this.TopOverlayY.setValue(0);
                      }
                    }}
                    data={user_goals}
                    renderItem={({item, index}) => {
                      return (
                        <DairyListItem
                          onPress={() => {
                            this.onPressGoal(item);
                          }}
                          onPressAddComment={() => {
                            this.props.navigation.navigate(Routes.AddCommentScreen, {
                              goal: item
                            })
                          }}
                          goal_id={item.id}
                          title={item.title}
                          daysPassed={1 + moment(new Date()).startOf('d').diff(moment(item.created_at).startOf("d"), 'days')}
                          daysForComplete={item.days_for_complete}
                        />
                      );
                    }}
                    bounces={false}
                    overScrollMode={"never"}
                    showsVerticalScrollIndicator={false}
                    ListHeaderComponent={<View style={{height: 20}}/>}
                    ListFooterComponent={<View ref={ref => this.FooterViewRef = ref} style={{height: 20}}/>}
                  />
                ) : (
                  <DairyEmpty />
                )}
                <View pointerEvents={"none"} style={{
                  position: 'absolute',
                  top: -1,
                  left: 0,
                  bottom: 0,
                  right: 0,
                }}>
                  <View onLayout={event => {
                    this.FooterViewRef && this.FooterViewRef.setNativeProps({style: {height: 20 + (event.nativeEvent.layout.height / 3)}})
                  }} style={{
                    flex: 1,
                    height: '100%',
                    justifyContent: 'flex-end',
                  }}>
                    <ListOverlay
                      width={Dimensions.get("window").width}
                    />
                  </View>
                </View>
                <View pointerEvents={"none"} style={{
                  position: 'absolute',
                  left: 0,
                  right: 0,
                  top: 0,
                  bottom: -1,
                  justifyContent: 'flex-start',
                }}>
                  <Animated.View
                    onLayout={(event: LayoutChangeEvent) => {
                      this.TopOverlayY.setValue(event.nativeEvent.layout.height);
                      this.TopOverlayHeight = event.nativeEvent.layout.height;
                      this.TopOverlayOpacity.setValue(1);
                    }}
                    style={{
                      opacity: this.TopOverlayOpacity,
                      transform: [
                        {rotate: '180deg'},
                        {translateY: this.TopOverlayY}
                      ],
                    }}
                  >
                    <ListOverlay
                      width={Dimensions.get("window").width}
                    />
                  </Animated.View>
                </View>
              </View>
              <DairyAddGoalButton
                onPressAddGoal={() => this.props.navigation.navigate(Routes.AddGoalScreen)}
                onPressGoToCompleted={() => this.props.navigation.navigate(Routes.CompletedGoalScreen)}
                hasCompleted={completed_goals && completed_goals.length > 0}
              />
            </View>

          )
        }}
      </LocalizationContext.Consumer>
    )
  }
};
