import * as React from "react";
import {LocalizationContext} from "../../../core/localization/useLocalization";
import {Dimensions, View} from "react-native";
import Text from "../../../core/components/Text";
import theme from "../../../core/style/theme";
import DairyEmptyIcon from "../icons/DairyEmptyIcon";

interface IProps {

}

export default class DairyEmpty extends React.Component<IProps, any>{
  private flowerHeight = Dimensions.get("window").height * 0.4;
  private flowerWidth = 295 * (this.flowerHeight / 350);

  render() {
    return (
      <LocalizationContext.Consumer>
        {localizedData => (
          <View style={{
            flex: 1,
            justifyContent: 'flex-end',
            alignItems: 'center',
            marginBottom: Dimensions.get("window").height * 0.06,
          }}>
            <Text style={{
              fontSize: 14,
              lineHeight: 16,
              color: theme.rgba(theme.white, 0.5),
            }}>
              {localizedData.dairy_screen.no_goals_with_comments}
            </Text>
            <DairyEmptyIcon
              height={this.flowerHeight}
              width={this.flowerWidth}
            />
          </View>
        )}
      </LocalizationContext.Consumer>
    );
  }
}
