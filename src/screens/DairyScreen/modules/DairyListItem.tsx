import * as React from 'react';
import {Dimensions, View, TouchableOpacity} from "react-native";
import theme from "../../../core/style/theme";
import Text from "../../../core/components/Text";
import useLocalization from "../../../core/localization/useLocalization";
import HasTodayComment from "../../../core/components/HasTodayComment/HasTodayComment";

interface IProps {
  onPress: () => void;
  onPressAddComment: () => void;

  title: string;
  goal_id: string;
  daysPassed: number;
  daysForComplete: number;
}

const DairyListItem: React.FunctionComponent<IProps> = props => {
  const {width} = Dimensions.get("window");
  const localizedData = useLocalization();

  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={props.onPress}
    >
    <View style={{
      marginTop: 10,
      width: '100%',
      paddingHorizontal: width * 0.1,
      paddingTop: width * 0.05,
      backgroundColor: theme.pink,
    }}>
      <Text style={{
        fontSize: 12,
        lineHeight: 14,
      }}>{props.title}</Text>
      <View style={{
        flexDirection: 'row',
        justifyContent: 'space-between'
      }}>
        <Text style={{
          fontSize: 10,
          lineHeight: 12,
          color: theme.green,
          marginVertical: width * 0.05,
        }}>{localizedData.home_screen.day} {props.daysPassed} <Text
          style={{
            color: theme.white,
            fontSize: 10,
            lineHeight: 12,
          }}>/ {props.daysForComplete}</Text></Text>
        <HasTodayComment goal_id={props.goal_id}>
          {(hasTodayComment: boolean) => {
            if (hasTodayComment) {
              return (
                <Text style={{
                  fontSize: 10,
                  lineHeight: 12,
                  color: theme.green,
                  marginVertical: width * 0.05,
                }}>{localizedData.dairy_screen.record_successfully_added}</Text>
              )
            }
            return (
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={props.onPressAddComment}
                style={{
                  paddingVertical: width * 0.05,
                  zIndex: 1,
                }}
              >
                <Text style={{
                  fontSize: 10,
                  lineHeight: 12,
                  color: theme.rgba(theme.white, 0.4)
                }}>{localizedData.home_screen.add_comment}</Text>
              </TouchableOpacity>
            )
          }}
        </HasTodayComment>
      </View>
    </View>
    </TouchableOpacity>
  );
}

export default DairyListItem;
