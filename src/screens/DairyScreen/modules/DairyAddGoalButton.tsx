import * as React from 'react';
import {Dimensions, TouchableOpacity, View} from "react-native";
import {LocalizationContext} from "../../../core/localization/useLocalization";
import RadialGradientView from "../../../core/components/RadialGradientView";
import theme from "../../../core/style/theme";
import Text from "../../../core/components/Text";

interface IProps {
  onPressAddGoal: () => void;
  onPressGoToCompleted: () => void;
  hasCompleted?: boolean;
}

interface IState {

}

export default class DairyAddGoalButton extends React.Component<IProps, IState>{
  render() {
    const {onPressAddGoal, onPressGoToCompleted, hasCompleted} = this.props;
    return (
      <LocalizationContext.Consumer>
        {localizedData => (
          <React.Fragment>
            <TouchableOpacity onPress={onPressAddGoal} style={{
              width: '100%',
              height: 44,
              alignItems: 'center',
              marginBottom: hasCompleted ? 0 : 40,
            }}>
              <View style={{alignItems: 'center'}}>
                <RadialGradientView width={Dimensions.get("window").width * 0.73}/>
                <View style={{
                  width: '100%',
                  height: 38,
                  alignItems: 'center',
                  justifyContent: 'center'
                }}>
                  <Text style={{
                    color: theme.green,
                    fontSize: 14,
                    lineHeight: 16,
                  }}>
                    {localizedData.home_screen.add_goal}
                  </Text>
                </View>
                <RadialGradientView width={Dimensions.get("window").width * 0.73}/>
              </View>
            </TouchableOpacity>
            {hasCompleted ? (
              <TouchableOpacity style={{
                width: '100%',
                height: 40,
              }} onPress={onPressGoToCompleted}>
                <View style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                  <Text style={{opacity: 0.4}}>{localizedData.dairy_screen.go_to_completed}</Text>
                </View>
              </TouchableOpacity>
            ) : null}
          </React.Fragment>
        )}
      </LocalizationContext.Consumer>
    );
  }
}
