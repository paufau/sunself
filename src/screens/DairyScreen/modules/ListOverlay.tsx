import React from "react";
import Svg, {Defs, LinearGradient, Path, Rect, Stop} from "react-native-svg";
import {IIconProps} from "../../../core/navigation/icons/DairyIcon";

const ListOverlay = (props: IIconProps) => {
  return (
    <Svg width="375" height="30" viewBox="0 0 375 30" fill="none">
      <Rect width="375" height="30" fill="url(#paint0_linear)"/>
      <Defs>
        <LinearGradient id="paint0_linear" x1="188" y1="0" x2="188" y2="30" gradientUnits="userSpaceOnUse">
          <Stop stopColor="#595269" stopOpacity="0"/>
          <Stop offset="0.71875" stopColor="#595269"/>
        </LinearGradient>
      </Defs>
    </Svg>
  );
}

export default ListOverlay;
